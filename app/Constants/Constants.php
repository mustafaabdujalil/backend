<?php

namespace App\Constants;

class Constants
{

    const INACTIVE_STATUS = 0;
    const ACTIVE_STATUS = 1;
    const DONE_STATUS = 2;
    const CANCELLED_STATUS = 3;


}
