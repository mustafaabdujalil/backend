<?php

use \App\Models\City;
use Intervention\Image\Facades\Image;
use \Illuminate\Support\Facades\Storage;

if (! function_exists('getCities')) {
    function getCities(int $id)
    {
        return City::where('country_id',$id)->get();
    }
}
