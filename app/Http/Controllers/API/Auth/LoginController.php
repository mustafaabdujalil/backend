<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserResource;
use App\Http\Traits\ResponseTrait;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use ResponseTrait;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {
        if(Auth::attempt($request->validated())){
            $user = User::where('email', $request->email)->first();
            $data['user'] = UserResource::make($user);
            $data['token'] = $user->createToken('git')->plainTextToken;
            return $this->success(__('User logged in successfully'), Response::HTTP_OK, $data);
        }
        return $this->success(__('User is unauthorized'), Response::HTTP_UNAUTHORIZED);
    }
}
