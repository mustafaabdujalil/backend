<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Http\Services\UserService;
use App\Http\Traits\ResponseTrait;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use ResponseTrait;

    private $userService;

    /**
     * Constructor
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService ) {
        $this->userService = $userService;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $user = $this->userService->createUser($request->validated());
        $data['user'] = UserResource::make($user);
        $data['token'] = $user->createToken('git')->plainTextToken;
        return $this->success(__('User created successfully'), Response::HTTP_OK, $data);
    }
}
