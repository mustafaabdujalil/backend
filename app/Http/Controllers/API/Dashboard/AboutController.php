<?php

namespace App\Http\Controllers\API\Dashboard;

use Exception;
use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Services\AboutService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\About\StoreRequest;
use App\Http\Requests\Dashboard\Admin\About\UpdateRequest;
use App\Http\Resources\AboutResource;

class AboutController extends Controller
{
    use ResponseTrait;

    private $aboutService;
    /**
     * Constructor
     *
     * @param AboutService $aboutService
    */
    public function __construct(AboutService $aboutService ) {
        $this->aboutService = $aboutService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = $this->aboutService->getAbout();
        return $this->success(null, Response::HTTP_OK, AboutResource::collection($about));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $aboutData = $request->validated();
        $about =  $this->aboutService->create($aboutData);
        return $this->success(__('Data created successfully'), Response::HTTP_OK, AboutResource::make($about));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $about = $this->aboutService->find($id);
            return $this->success(null, Response::HTTP_OK, AboutResource::make($about));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $about = $this->aboutService->update($request, $id);
            return $this->success(__('Record updated successfully'), Response::HTTP_OK, AboutResource::make($about));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $about = $this->aboutService->delete($id);
            return $this->success(__('Record deleted successfully'), Response::HTTP_OK, $about);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }
}
