<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\Category\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Category\UpdateRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Services\CategoryService;
use App\Http\Traits\ResponseTrait;
use Illuminate\Http\Response;
use Exception;

class CategoryController extends Controller
{
    use ResponseTrait;

    private $categoryService;

    /**
     * Constructor
     *
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService ) {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryService->getCategories();
        return $this->success(null, Response::HTTP_OK, CategoryResource::collection($categories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $categoryData = $request->validated();
        $category =  $this->categoryService->createCategory($categoryData);
        return $this->success(__('Category created successfully'), Response::HTTP_OK, CategoryResource::make($category));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $category = $this->categoryService->findCategory($id);
            return $this->success(null, Response::HTTP_OK, CategoryResource::make($category));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $category = $this->categoryService->update($request->validated(), $id);
            return $this->success(__('Category updated successfully'), Response::HTTP_OK, CategoryResource::make($category));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = $this->categoryService->delete($id);
            return $this->success(__('Category deleted successfully'), Response::HTTP_OK, $category);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }
}
