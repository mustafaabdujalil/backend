<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Requests\Website\ContactUsMessageRequest;
use App\Models\ContactUsMessage;
use Exception;
use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Services\ContactService;
use App\Http\Resources\ContactResource;
use App\Http\Resources\ContactUsMessageResource;
use App\Http\Requests\Dashboard\Admin\Contact\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Contact\UpdateRequest;

class ContactUsController extends Controller
{
    use ResponseTrait;

    private $contactService;

    /**
     * Constructor
     *
     * @param ContactService contactService
    */
    public function __construct(ContactService $contactService ) {
        $this->contactService = $contactService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = $this->contactService->getContactUs();
        return $this->success(null, Response::HTTP_OK, ContactResource::make($contact));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $contactData = $request->validated();
        $contact =  $this->contactService->create($contactData);
        return $this->success(__('Data created successfully'), Response::HTTP_OK, ContactResource::make($contact));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $contact = $this->contactService->find($id);
            return $this->success(null, Response::HTTP_OK, $contact);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $contact = $this->contactService->update($request, $id);
            return $this->success(__('Record updated successfully'), Response::HTTP_OK, ContactResource::make($contact));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $contact = $this->contactService->delete($id);
            return $this->success(__('Record deleted successfully'), Response::HTTP_OK, $contact);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * List contact-us website messages
     *
     * @return \Illuminate\Http\Response
     */
    public function listContactUsMessage()
    {
        $messages = $this->contactService->getContactUsMessages();
        return $this->success(null, Response::HTTP_OK, ContactUsMessageResource::collection($messages));
    }

    public function storeContactUsMessage(ContactUsMessageRequest $request){
        ContactUsMessage::create($request->validated());
        return $this->success(__('Data created successfully'), Response::HTTP_OK);
    }
}
