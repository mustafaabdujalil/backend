<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\Event\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Event\UpdateRequest;
use App\Http\Resources\EventResource;
use App\Http\Services\EventService;
use App\Http\Traits\ResponseTrait;
use Illuminate\Http\Response;
use Exception;

class EventController extends Controller
{
    use ResponseTrait;

    private $eventService;

    /**
     * Constructor
     *
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService ) {
        $this->eventService = $eventService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = $this->eventService->getEvents();
        return $this->success(null, Response::HTTP_OK, EventResource::collection($events));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $eventData = $request->validated();
        $event =  $this->eventService->createEvent($eventData);
        return $this->success(__('Event created successfully'), Response::HTTP_OK, EventResource::make($event));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $event = $this->eventService->findEvent($id);
            return $this->success(null, Response::HTTP_OK, EventResource::make($event));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,Response::HTTP_FORBIDDEN);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $event = $this->eventService->update($request->validated(), $id);
            return $this->success(__('Event updated successfully'), Response::HTTP_OK, EventResource::make($event));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $event = $this->eventService->delete($id);
            return $this->success(__('Event deleted successfully'), Response::HTTP_OK, $event);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,Response::HTTP_FORBIDDEN);
        }
    }
}
