<?php

namespace App\Http\Controllers\API\Dashboard;

use Exception;
use Illuminate\Http\Response;
use App\Http\Services\GiftService;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\Gift\ListRequest;
use App\Http\Resources\GiftResource;
use App\Http\Requests\Dashboard\Admin\Gift\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Gift\UpdateRequest;


class GiftController extends Controller
{
    use ResponseTrait;

    private $giftService;

    /**
     * Constructor
     *
     * @param giftService $giftService
     */
    public function __construct(GiftService $giftService ) {
        $this->giftService = $giftService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListRequest $request)
    {
        $gifts = $this->giftService->getGifts($request->validated());
        return $this->success(null, Response::HTTP_OK, GiftResource::collection($gifts));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $giftData = $request->validated();
        $gift =  $this->giftService->createGift($giftData);
        return $this->success(__('Gift created successfully'), Response::HTTP_OK, GiftResource::make($gift));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $gift = $this->giftService->findGift($id);
            return $this->success(null, Response::HTTP_OK, GiftResource::make($gift));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $gift = $this->giftService->update($request->validated(), $id);
            return $this->success(__('Gift updated successfully'), Response::HTTP_OK, GiftResource::make($gift));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $gift = $this->giftService->delete($id);
            return $this->success(__('Gift deleted successfully'), Response::HTTP_OK, $gift);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,Response::HTTP_FORBIDDEN);
        }
    }
}
