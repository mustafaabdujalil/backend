<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\Host\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Host\UpdateRequest;
use App\Http\Resources\HostResource;
use App\Http\Services\HostService;
use App\Http\Traits\ResponseTrait;
use Illuminate\Http\Response;
use Exception;

class HostController extends Controller
{
    use ResponseTrait;

    private $hostService;

    /**
     * Constructor
     *
     * @param HostService $hostService
     */
    public function __construct(HostService $hostService ) {
        $this->hostService = $hostService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hosts = $this->hostService->getHosts();
        return $this->success(null, Response::HTTP_OK, HostResource::collection($hosts));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $hostData = $request->validated();
        $host =  $this->hostService->createHost($hostData);
        return $this->success(__('Host created successfully'), Response::HTTP_OK, HostResource::make($host));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Host  $host
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $host = $this->hostService->findHost($id);
            return $this->success(null, Response::HTTP_OK, HostResource::make($host));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $host = $this->hostService->update($request->validated(), $id);
            return $this->success(__('Host updated successfully'), Response::HTTP_OK, HostResource::make($host));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $host
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $host = $this->hostService->delete($id);
            return $this->success(__('Host deleted successfully'), Response::HTTP_OK, $host);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }
}
