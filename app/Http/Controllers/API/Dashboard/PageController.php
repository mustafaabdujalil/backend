<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Resources\PageResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Services\PageService;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Admin\Pages\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Pages\UpdateRequest;

class PageController extends Controller
{


    use ResponseTrait;

    private $pageService;

    /**
     * Constructor
     *
     * @param PageService pageService
    */
    public function __construct(PageService $pageService ) {
        $this->pageService = $pageService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = $this->pageService->getPages();
        return $this->success(null, Response::HTTP_OK, PageResource::collection($pages));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $pageData = $request->validated();
        $page =  $this->pageService->create($pageData);
        return $this->success(__('Page created successfully'), Response::HTTP_OK, $page);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $page = $this->pageService->find($id);
            return $this->success(null, Response::HTTP_OK, PageResource::make($page));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $page = $this->pageService->update($request->validated(), $id);
            return $this->success(__('Page updated successfully'), Response::HTTP_OK, $page);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $page = $this->pageService->delete($id);
            return $this->success(__('Page deleted successfully'), Response::HTTP_OK, $page);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }
}
