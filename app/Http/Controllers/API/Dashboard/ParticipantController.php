<?php

namespace App\Http\Controllers\API\Dashboard;

use Exception;
use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Services\ParticipantService;
use App\Http\Resources\ParticipantResource;
use App\Http\Requests\Dashboard\Admin\Participants\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Participants\UpdateRequest;

class ParticipantController extends Controller
{

    use ResponseTrait;

    private $participantService;

    /**
     * Constructor
     *
     * @param ParticipantService $participantService
    */
    public function __construct(ParticipantService $participantService ) {
        $this->participantService = $participantService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participant = $this->participantService->getParticipants();
        return $this->success(__('success'), Response::HTTP_OK, ParticipantResource::collection($participant));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $participantData = $request->validated();
        $participant =  $this->participantService->createParticipant($participantData);
        return $this->success(__('Participant created successfully'), Response::HTTP_OK, ParticipantResource::make($participant));
    }

    /**
     * Display the specified resource.
     *
     * @param [type] $id
     * \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $participant = $this->participantService->findParticipant($id);
            return $this->success(__('success'), Response::HTTP_OK, ParticipantResource::make($participant));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $participant = $this->participantService->update($request->validated(), $id);
            return $this->success(__('Participant updated successfully'), Response::HTTP_OK, ParticipantResource::make($participant));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $participant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $participant = $this->participantService->delete($id);
            return $this->success(__('{Participant deleted successfully'), Response::HTTP_OK, $participant);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }
}
