<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Resources\SliderResource;
use Exception;
use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Services\SliderService;
use App\Http\Requests\Dashboard\Admin\Slider\StoreRequest;
use App\Http\Requests\Dashboard\Admin\Slider\UpdateRequest;

class SliderController extends Controller
{
    use ResponseTrait;

    private $sliderService;

    /**
     * Constructor
     *
     * @param SliderService $sliderService
     */
    public function __construct(SliderService $sliderService ) {
        $this->sliderService = $sliderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = $this->sliderService->getSliders();
        return $this->success(null, Response::HTTP_OK, SliderResource::collection($sliders));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $sliderData = $request->all();
        $slider =  $this->sliderService->createSlider($sliderData);
        return $this->success(__('Slider created successfully'), Response::HTTP_OK, SliderResource::make($slider));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $slider = $this->sliderService->findSlider($id);
            return $this->success(null, Response::HTTP_OK, SliderResource::make($slider));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $slider = $this->sliderService->update($request, $id);
            return $this->success(__('Slider updated successfully'), Response::HTTP_OK, SliderResource::make($slider));
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $slider = $this->sliderService->delete($id);
            return $this->success(__('Slider deleted successfully'), Response::HTTP_OK, $slider);
        } catch (Exception $ex) {
            return $this->error($ex->getMessage() ,$ex->getCode());
        }
    }
}
