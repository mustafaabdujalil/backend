<?php

namespace App\Http\Controllers\API\Website;

use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Services\AboutService;
use App\Http\Controllers\Controller;
use App\Http\Resources\AboutResource;

class AboutController extends Controller
{
    use ResponseTrait;

    private $aboutService;

    /**
     * Constructor
     *
     * @param AboutService $aboutService
    */
    public function __construct(AboutService $aboutService ) {
        $this->aboutService = $aboutService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about = $this->aboutService->getWebsiteAboutUs();
        return $this->success(null, Response::HTTP_OK, AboutResource::make($about));
    }
}
