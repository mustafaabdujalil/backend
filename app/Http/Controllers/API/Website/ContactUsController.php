<?php

namespace App\Http\Controllers\API\Website;

use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Services\ContactService;
use App\Http\Resources\ContactResource;
use App\Http\Requests\Website\ContactUsMessageRequest;

class ContactUsController extends Controller
{
    use ResponseTrait;

    private $contactService;

    /**
     * Constructor
     *
     * @param ContactService contactService
    */
    public function __construct(ContactService $contactService ) {
        $this->contactService = $contactService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = $this->contactService->getWebsiteContactUs();
        return $this->success(null, Response::HTTP_OK, ContactResource::make($contact));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeContactUsMessage(ContactUsMessageRequest $request)
    {
        $messageData = $request->validated();
        $this->contactService->storeContactUsMessage($messageData);
        return $this->success(__('Message sent successfully'), Response::HTTP_OK);
    }


}
