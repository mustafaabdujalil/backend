<?php

namespace App\Http\Controllers\API\Website;

use App\Http\Requests\Website\HomeEventFilterRequest;
use App\Http\Resources\AdsResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\GiftResource;
use App\Http\Services\CategoryService;
use App\Http\Services\GiftService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Traits\ResponseTrait;
use App\Http\Services\AboutService;
use App\Http\Services\EventService;
use App\Http\Controllers\Controller;
use App\Http\Services\SliderService;
use App\Http\Resources\AboutResource;
use App\Http\Resources\EventResource;
use App\Http\Resources\SliderResource;

class HomeController extends Controller
{
    use ResponseTrait;

    private $aboutService;
    private $sliderService;
    private $eventService;
    private $categoryService;
    private $giftService;

    /**
     * Undocumented function
     *
     * @param AboutService $aboutService
     * @param SliderService $sliderService
     * @param EventService $eventService
     * @param CategoryService $categoryService
     * @param GiftService $giftService
    */
    public function __construct(AboutService $aboutService, SliderService $sliderService,
                                EventService $eventService, CategoryService $categoryService,
                                GiftService $giftService) {
        $this->aboutService = $aboutService;
        $this->sliderService = $sliderService;
        $this->eventService = $eventService;
        $this->categoryService = $categoryService;
        $this->giftService = $giftService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HomeEventFilterRequest $request)
    {
        $about = $this->aboutService->getAbout();
        $slider = $this->sliderService->getSliders();
        $events = $this->eventService->getEvents($request->validated());
        $categories = $this->categoryService->getCategories();
        $completedGifts = $this->giftService->getGifts(['status' => 1]);
        $giftAds= $this->giftService->getGifts(['order_col' => 'created_at','order_type' => 'desc'])
                                    ->take(2);

        $home = [
            'slider' => SliderResource::collection($slider),
            'about' => AboutResource::collection($about),
            'events' => EventResource::collection($events),
            'categories' => CategoryResource::collection($categories),
            'completed_gifts' => GiftResource::collection($completedGifts),
            'gift_ads' => AdsResource::collection($giftAds),
        ];
        return $this->success(null, Response::HTTP_OK, $home);
    }
}
