<?php

namespace App\Http\Controllers;

use App\Http\Services\AboutService;
use App\Http\Services\ContactService;

class AboutController extends Controller
{
    private $aboutService;
    public function __construct(AboutService $aboutService)
    {
        $this->aboutService = $aboutService;
    }

    public function index()
    {
        $about = $this->aboutService->getWebsiteAboutUs();
        return view('about', compact('about'));
    }

}
