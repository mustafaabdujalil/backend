<?php

namespace App\Http\Controllers;

use App\Models\ContactUsMessage;
use App\Models\Gift;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index()
    {
        $cartItems = $this->getCartItemsMergedWithPaidAndRemainingInfo();
        if(Auth::check()){
            cart()->setUser(\auth()->id());
        }
        return view('cart',compact('cartItems'));
    }

    public function create(Request $request)
    {
        try {
            Gift::addToCart($request->gift_id);
            if(Auth::check()){
                cart()->setUser(\auth()->id());
            }
            Toastr::success(__('Added to cart successfully'), __('success'));
            return redirect()->route('cart.index');
        }catch (\Exception $exception){
            Toastr::error(__('error'), __('error'));
            return back();
        }

    }

    public function getCartItemsMergedWithPaidAndRemainingInfo()
    {
        $gifts = Gift::whereIn('id', array_column(cart()->items(),'modelId'))->get();
        $cartItems = cart()->items();
        if(Auth::check()){
            cart()->setUser(\auth()->id());
        }
        foreach($gifts as $gift)
        {
            foreach ($cartItems as &$item){
                if($item['modelId'] == $gift->id){
                    $item['paid'] = $gift->paid;
                    $item['remaining'] = $gift->remaining;
                }
            }
        }

        return $cartItems;
    }

    public function destroy($cartItemIndex)
    {
        try {
            cart()->removeAt((int)$cartItemIndex);
            Toastr::success(__('Removed from cart successfully'), __('success'));
            return redirect()->route('cart.index');
        }catch (\Exception $exception){
            Toastr::error(__('error'), __('error'));
            return back();
        }
    }
}
