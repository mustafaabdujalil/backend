<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Http\Services\ContactService;
use App\Models\ContactUsMessage;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;

class ContactController extends Controller
{
    private $contactService;
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function index()
    {
        $contact = $this->contactService->getWebsiteContactUs();
        return view('contact', compact('contact'));
    }

    public function storeContact(ContactRequest $request)
    {
        try {
            ContactUsMessage::create($request->validated());
            Toastr::success(__('Sent successfully'));
        }catch (\Exception $exception){
            Toastr::error(__('error'), __('error'));
        }

        return redirect()->route('contact-us');
    }

}
