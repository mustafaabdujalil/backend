<?php

namespace App\Http\Controllers;

use App\Http\Services\EventService;
use App\Http\Services\SliderService;
use App\Http\Services\UserService;
use App\Models\Event;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class EventController extends Controller
{
    private $eventService;
    public function __construct(UserService $userService, EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    public function list(Request $request)
    {
        $events = $this->eventService->getEvents($request->all(), true);
        $events->appends($request->all());
        $viewName = $request->has('type') ? ( $request->type == Event::PRIVATE_EVENT ? "event.private-events" : "event.public-events") : "event.events";

        return view($viewName, compact('events'));
    }

    public function show($slug, Request $request)
    {
        $event = $this->eventService->findEventBySlug($slug);
        if($event->type == Event::PRIVATE_EVENT){
            if(!$request->has('password') || $request->password != $event->password){
                Toastr::error(__('you need to enter event valid password'), __('error'));
                return redirect()->back();
            }
        }

        return view('event.view', compact('event'));
    }
}
