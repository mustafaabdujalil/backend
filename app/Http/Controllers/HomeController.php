<?php

namespace App\Http\Controllers;

use App\Constants\Constants;
use App\Http\Services\EventService;
use App\Http\Services\GiftService;
use App\Http\Services\SliderService;
use App\Models\Event;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $sliderService, $eventService, $giftService;
    public function __construct(SliderService $sliderService, EventService $eventService, GiftService $giftService)
    {
        $this->sliderService = $sliderService;
        $this->eventService = $eventService;
        $this->giftService = $giftService;
    }

    public function index(Request $request)
    {
        $slider = $this->sliderService->getSliders();
        $events = $this->eventService->getEvents($request->all());
        $gifts = $this->giftService->getGifts(['status' => Constants::ACTIVE_STATUS]);
        $privateEvents = $this->eventService->getEvents(['type' => Event::PRIVATE_EVENT]);

        return view('home', compact('slider', 'events', 'gifts', 'privateEvents'));
    }

}
