<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\HostResource;
use App\Http\Services\CityService;
use App\Http\Services\CountryService;
use App\Http\Services\HostService;
use App\Http\Services\PageService;
use App\Http\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ListsController extends Controller
{
    use ResponseTrait;

    private $hostService;
    private $countryService;
    private $cityService;
    private $pageService;

    /**
     * Constructor
     *
     * @param HostService $hostService
     * @param CountryService $countryService
     * @param CityService $cityService
     * @param PageService $pageService
     */
    public function __construct(HostService $hostService, CountryService $countryService,
                                CityService $cityService, PageService $pageService) {
        $this->hostService = $hostService;
        $this->countryService = $countryService;
        $this->cityService = $cityService;
        $this->pageService = $pageService;
    }

    // return hosts list
    public function hosts(){
        $hosts = $this->hostService->getHosts();
        return $this->success(__('success'), Response::HTTP_OK, HostResource::collection($hosts));
    }

    // return countries list
    public function countries(){
        $countries = $this->countryService->getCountries();
        return $this->success(__('success'), Response::HTTP_OK, CountryResource::collection($countries));
    }

    // return cities list
    public function cities(Request $request){
        $cities = $this->cityService->getCities($request->country_id);
        return $this->success(__('success'), Response::HTTP_OK, CityResource::collection($cities));
    }

    // return pages list
    public function pages(Request $request){
        $pages = $this->pageService->getPages();
        return $this->success(__('success'), Response::HTTP_OK, $pages);
    }
}
