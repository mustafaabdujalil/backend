<?php

namespace App\Http\Controllers;

use App\Http\Services\OrderService;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public $orderService;
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->orderService->getOrders(['client_id' => auth()->id()]);
        return view('user.orders', compact('orders'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->orderService->createOrder($request->all());
            DB::commit();
            cart()->clear();
            Toastr::success(__('Order created successfully'), __('success'));
            return redirect()->route('index');
        }catch (\Exception $exception){
            DB::rollBack();
            Toastr::error($exception->getMessage(), __('error'));
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
