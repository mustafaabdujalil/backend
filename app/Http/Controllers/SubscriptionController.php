<?php

namespace App\Http\Controllers;

use App\Http\Requests\Website\SubscriptionRequest;
use App\Models\Newsletter;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscriptionController extends Controller
{
    public function subscribe(SubscriptionRequest $request){

        try {
            DB::beginTransaction();
            Newsletter::create([
                'email' => $request->email
            ]);
            DB::commit();
            Toastr::success(__('Subscribed successfully'), __('success'));
            return redirect()->route('index');
        }catch (\Exception $exception){
            DB::rollBack();
            Toastr::error(__('error'), __('error'));
            return back();
        }
    }
}
