<?php

namespace App\Http\Controllers;

use App\Http\Requests\Website\UpdateProfileRequest;
use App\Http\Services\EventService;
use App\Http\Services\SliderService;
use App\Http\Services\UserService;
use App\Models\City;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userService, $eventService;
    public function __construct(UserService $userService, EventService $eventService)
    {
        $this->userService = $userService;
        $this->eventService = $eventService;
    }

    public function index(Request $request)
    {
        $user = $this->userService->findUser(auth()->user()->id);
        $cities = [];
        if($user->city)
            $cities = City::where('country_id', $user->city->country_id)->get();

        return view('user.profile', compact('user','cities'));
    }


    public function update(UpdateProfileRequest $request)
    {
        if($request->user_id != auth()->id()){
            Toastr::error(__('Allowed dined'), __('error'));
            return back();
        }
        $user = $this->userService->update(auth()->id(), $request->validated());
        Toastr::success(__('Update successfully'), __('success'));

        return redirect()->route('profile');


    }

}
