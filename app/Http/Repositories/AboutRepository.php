<?php

namespace App\Http\Repositories;

use App\Models\About;

class AboutRepository{


    public function getAbout()
    {
        return About::get();
    }


    public function create($data)
    {
        $about =  About::create($data);
        $about->addMediaFromRequest('image')->toMediaCollection('image');
        return $about;
    }

    public function find($id)
    {
        $about = About::find($id);
        if(!$about) {
            throw new \Exception("Record Not Found", 404);
        }
        return $about;
    }

    public function update($request, $id)
    {
        $about = $this->find($id);
        $about->update($request->all());
        if($request->image){
            $about->clearMediaCollection('image');
            $about->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $about;
    }

    public function delete($id)
    {
        $about = $this->find($id);
        $about->delete();
        return true;
    }

    /**
     * Get about-us first record for website
     *
     * @return collection
    */
    public function getWebsiteAboutUs()
    {
        return About::first();
    }
}
