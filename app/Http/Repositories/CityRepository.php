<?php

namespace App\Http\Repositories;

use App\Models\City;

class CityRepository{

    public function getCities($countryId)
    {
        return City::where('country_id', $countryId)->get();
    }
}
