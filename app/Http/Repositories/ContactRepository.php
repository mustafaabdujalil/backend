<?php

namespace App\Http\Repositories;

use App\Models\Contact;
use App\Models\ContactUsMessage;

class ContactRepository{


    public function getContactUs()
    {
        return Contact::get();
    }


    public function create($data)
    {
        $contact =  Contact::create($data);
        return $contact;
    }

    public function find($id)
    {
        $contact = Contact::find($id);
        if(!$contact) {
            throw new \Exception("Record Not Found", 404);
        }
        return $contact;
    }

    public function update($request, $id)
    {
        $contact = $this->find($id);
        $contact->update($request->all());
        return $contact;
    }

    public function delete($id)
    {
        $contact = $this->find($id);
        $contact->delete();
        return true;
    }

    /**
     * Get contact-us first record for website
     *
     * @return collection
    */
    public function getWebsiteContactUs()
    {
        return Contact::first();
    }

    /**
     * Save contact-us website message
     *
     * @return collection
    */
    public function storeContactUsMessage($message)
    {
        $message =  ContactUsMessage::create($message);
        return $message;
    }

    /**
     * List contact-us website messages
     *
     * @return collection
    */
    public function getContactUsMessages()
    {
        return ContactUsMessage::get();
    }
}
