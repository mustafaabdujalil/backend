<?php

namespace App\Http\Repositories;

use App\Models\Country;

class CountryRepository{

    public function getCountries()
    {
        return Country::get();
    }
}
