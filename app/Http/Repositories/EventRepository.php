<?php

namespace App\Http\Repositories;

use App\Models\Event;
use Illuminate\Http\Response;

class EventRepository{


    public function getEvents($filter = [])
    {
        return Event::with('category','hostCountry','hostCity', 'gifts')
            ->when(isset($filter['key']), function ($query) use ($filter){
                $query->where('name_en', 'LIKE', '%'.$filter['key'].'%')
                    ->orWhere('name_ar', 'LIKE', '%'.$filter['key'].'%')
                    ->orWhere('description_en', 'LIKE', '%'.$filter['key'].'%')
                    ->orWhere('description_ar', 'LIKE', '%'.$filter['key'].'%');
            })
            ->when(isset($filter['category_id']), function ($query) use ($filter){
                $query->where('category_id', $filter['category_id']);
            })->get();
    }


    public function createEvent($eventData)
    {
        $event =  Event::create($eventData);
        $event->addMediaFromRequest('image')->toMediaCollection('image');
        return $event;
    }

    public function findEvent($id)
    {
        $event = Event::with('category','hostCountry','hostCity')
                        ->where('id', $id)->first();
        if(!$event) {
            throw new \Exception("Event Not Found", Response::HTTP_NOT_FOUND);
        }
        return $event;
    }

    public function update($data, $id)
    {
        $event = $this->findEvent($id);
        Event::where('id', $id)->update($data);
        if(request()->has('image')){
            $event->clearMediaCollection('image');
            $event->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $event;
    }

    public function delete($id)
    {
        $event = $this->findEvent($id);
        $event->delete();
        return true;
    }
}
