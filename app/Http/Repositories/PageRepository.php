<?php

namespace App\Http\Repositories;

use App\Models\Page;

class PageRepository{


    public function getPages()
    {
        return Page::get();
    }


    public function create($data)
    {
        unset($data['image']);
        $page =  Page::create($data);
        if(request()->has('image')) {
            $page->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $page;
    }

    public function find($id)
    {
        $page = Page::find($id);
        if(!$page) {
            throw new \Exception("Page Not Found", 404);
        }
        return $page;
    }

    public function update($data, $id)
    {
        $page = $this->find($id);
        unset($data['image']);
        $page->update($data);
        if(request()->has('image')){
            $page->clearMediaCollection('image');
            $page->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $page;
    }

    public function delete($id)
    {
        $page = $this->find($id);
        $page->delete();
        return true;
    }
}
