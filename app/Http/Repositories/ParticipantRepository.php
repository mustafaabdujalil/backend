<?php

namespace App\Http\Repositories;

use App\Models\Participant;

class ParticipantRepository{


    public function getParticipants($pagination = false)
    {
        if($pagination){
            return Participant::query()->paginate(config('app.pagination_count'));
        }
        return Participant::get();
    }


    public function createParticipant($participantData)
    {
        $participant =  Participant::create($participantData);
        return $participant;
    }

    public function findParticipant($id)
    {
        $participant = Participant::find($id);
        if(!$participant) {
            throw new \Exception("Participant Not Found", 404);
        }
        return $participant;
    }

    public function update($data, $id)
    {
        $participant = $this->findParticipant($id);
        $participant->update($data);
        return $participant;
    }

    public function delete($id)
    {
        $participant = $this->findParticipant($id);
        $participant->delete();
        return true;
    }
}
