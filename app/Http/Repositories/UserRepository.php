<?php

namespace App\Http\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository{

    public function getUsers()
    {
        return User::get();
    }


    public function createUser($userData)
    {
        if(isset($userData['password'])){
            $userData['password'] = Hash::make($userData['password']);
        }
        $user =  User::create($userData);
        return $user;
    }

    public function findUser($id)
    {
        $user = User::with('city.country')->where('id', $id)->first();
        if(!$user) {
            throw new \Exception("User Not Found", 404);
        }
        return $user;
    }

    public function update($data, $id)
    {
        $data['name'] = $data['first_name'].' '.$data['last_name'];
        User::where('id', $id)->update($data);
        return $this->findUser($id);
    }

    public function delete($id)
    {
        $user = $this->findUser($id);
        $user->delete();
        return true;
    }
}
