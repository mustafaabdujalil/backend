<?php

namespace App\Http\Requests\Dashboard\Admin\About;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'nullable|string',
            'content_en' => 'nullable|string',
            'title_ar' => 'nullable|string',
            'content_ar' => 'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}
