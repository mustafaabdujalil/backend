<?php

namespace App\Http\Requests\Dashboard\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'nullable|string',
            'description_en' => 'nullable|string',
            'name_ar' => 'nullable|string',
            'description_ar' => 'nullable|string',
        ];
    }
}
