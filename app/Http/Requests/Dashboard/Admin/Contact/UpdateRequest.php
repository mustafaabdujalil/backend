<?php

namespace App\Http\Requests\Dashboard\Admin\Contact;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_en' => 'nullable|string',
            'contact_ar' => 'nullable|string',
            'address_en' => 'nullable|string',
            'address_ar' => 'nullable|string',
            'email_1' => 'nullable|email',
            'email_2' => 'nullable|email',
            'phone_1' => 'nullable|regex:/(01)[0-9]{9}/',
            'phone_2' => 'nullable|regex:/(01)[0-9]{9}/',
            'facebook'  => 'nullable|string',
            'youtube'   => 'nullable|string',
            'twitter'   => 'nullable|string',
            'instagram' => 'nullable|string'
        ];
    }
}
