<?php

namespace App\Http\Requests\Dashboard\Admin\Event;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|string|max:255',
            'description_en' => 'required|string',
            'name_ar' => 'nullable|string|max:255',
            'description_ar' => 'nullable|string',
            'type' => 'required|string|in:public,private',
            'password' => 'nullable|string|min:6|max:15',
            'date' => 'required|date_format:Y-m-d|after_or_equal:today',
            'category_id' => 'required|numeric|exists:categories,id',
            'is_gift_card_allowed' => 'required|numeric|in:0,1',
            'host_name' => 'required|string|max:255',
            'host_code' => 'nullable|string|max:255',
            'host_country_id' => 'required||numeric|exists:countries,id',
            'host_city_id' => 'required||numeric|exists:cities,id',
            'host_id' => 'required||numeric|exists:users,id',
            'host_address' => 'required|string|max:255',
        ];
    }
}
