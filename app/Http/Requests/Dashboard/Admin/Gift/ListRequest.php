<?php

namespace App\Http\Requests\Dashboard\Admin\Gift;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_gift_card' => 'nullable|numeric|in:0,1',
        ];
    }
}
