<?php

namespace App\Http\Requests\Dashboard\Admin\Gift;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|string|max:255',
            'description_en' => 'required|string',
            'name_ar' => 'nullable|string|max:255',
            'description_ar' => 'nullable|string',
            'price' => 'required|numeric|gt:0',
            'status' => 'required|numeric|in:0,1',
            'is_gift_card' => 'required|numeric|in:0,1',
            'event_id' => 'required|numeric|exists:events,id',
            'image' => 'required|image|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}
