<?php

namespace App\Http\Requests\Dashboard\Admin\Gift;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'nullable|string|max:255',
            'description_en' => 'nullable|string',
            'name_ar' => 'nullable|string|max:255',
            'description_ar' => 'nullable|string',
            'price' => 'nullable|numeric|gt:0',
            'status' => 'nullable|numeric|in:0,1',
            'is_gift_card' => 'nullable|numeric|in:0,1',
            'event_id' => 'nullable|numeric|exists:events,id',
            'image' => 'nullable|image|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}
