<?php

namespace App\Http\Requests\Dashboard\Admin\Pages;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|string',
            'name_ar' => 'nullable|string',
            'url'     => 'required|string',
            'position'     => 'required|string',
            'details_en' => 'required|string',
            'details_ar' => 'nullable|string',
            'image' => 'nullable|image|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}
