<?php

namespace App\Http\Requests\Dashboard\Admin\Slider;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'required|string',
            'content_en' => 'required|string',
            'title_ar' => 'nullable|string',
            'content_ar' => 'nullable|string',
            'url' => 'nullable|url',
            'image' => 'required|image|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}
