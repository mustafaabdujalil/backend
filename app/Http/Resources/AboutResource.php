<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $title = 'title_'.strtolower(app()->getLocale() ?? 'en');
        $content = 'content_'.strtolower(app()->getLocale() ?? 'en');
        return [
            'id' => $this->id,
            'title' => $this->$title,
            'content' => $this->$content,
            'image' => $this->getFirstMediaUrl('image'),
        ];
    }
}
