<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $name = 'name_'.strtolower(app()->getLocale() ?? 'en');
        $description = 'description_'.strtolower(app()->getLocale() ?? 'en');
        return [
            'id' => $this->id,
            'name' => $this->$name,
            'description' => $this->$description
        ];
    }
}
