<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $contact = 'contact_'.strtolower(app()->getLocale() ?? 'en');
        $address = 'address_'.strtolower(app()->getLocale() ?? 'en');
        return [
            'id'    => $this->id,
            'contact' => $this->$contact,
            'address' => $this->$address,
            'email_1' => $this->email_1,
            'email_2' => $this->email_2,
            'phone_1' => $this->phone_1,
            'phone_2' => $this->phone_2,
            'facebook' => $this->facebook,
            'youtube' => $this->youtube,
            'twitter' => $this->twitter,
            'instagram' => $this->instagram,
        ];
    }
}
