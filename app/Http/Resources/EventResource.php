<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $name = 'name_'.strtolower(app()->getLocale() ?? 'en');
        $description = 'description_'.strtolower(app()->getLocale() ?? 'en');
        return [
            'id' => $this->id,
            'name' => $this->$name,
            'description' => $this->$description,
            'date' => $this->date,
            'category' => CategoryResource::make($this->category),
            'type' => $this->type,
            'image' => $this->getFirstMediaUrl('image'),
            'is_gift_card_allowed' => $this->is_gift_card_allowed,
            'host_name' => $this->host_name,
            'host_code' => $this->host_code,
            'host' => HostResource::make($this->host),
            'host_country' => CountryResource::make($this->hostCountry),
            'host_city' => CityResource::make($this->hostCity),
            'host_address' => $this->host_address,
            'gifts' => GiftResource::collection($this->whenLoaded('gifts')),
        ];
    }
}
