<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GiftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $name = 'name_'.strtolower(app()->getLocale() ?? 'en');
        $description = 'description_'.strtolower(app()->getLocale() ?? 'en');
        return [
            'id' => $this->id,
            'name' => $this->$name,
            'description' => $this->$description,
            'price' => $this->price,
            'status' => $this->status,
            'image' => $this->getFirstMediaUrl('image'),
            'is_gift_card' => $this->is_gift_card,
            'event' => EventResource::make($this->whenLoaded('event')),
            'creator' => UserResource::make($this->creator),
            'participants' => UserResource::collection($this->whenLoaded('participants'))
        ];
    }
}
