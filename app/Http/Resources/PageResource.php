<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $name = 'name_'.strtolower(app()->getLocale() ?? 'en');
        $details = 'details_'.strtolower(app()->getLocale() ?? 'en');
        return [
            'id' => $this->id,
            'name' => $this->$name,
            'details' => $this->$details,
            'url' => $this->url,
            'position' => $this->position,
            'image' => $this->getFirstMediaUrl('image')
        ];
    }
}
