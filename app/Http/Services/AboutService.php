<?php

namespace App\Http\Services;

use App\Http\Repositories\AboutRepository;
use App\Models\About;

class AboutService{

    private $aboutRepository;
    public function __construct(AboutRepository $aboutRepository)
    {
        $this->aboutRepository = $aboutRepository;
    }

    public function getAbout()
    {
        return $this->getAbout();
    }


    public function create($data)
    {
        return $this->aboutRepository->create($data);
    }

    public function find($id)
    {
        return $this->aboutRepository->find($id);
    }

    public function update($request, $id)
    {
        return $this->aboutRepository->update($request, $id);
    }

    public function delete($id)
    {
        return $this->aboutRepository->delete($id);
    }

    /**
     * Get about-us first record for website
     *
     * @return collection
    */
    public function getWebsiteAboutUs()
    {
        return $this->aboutRepository->getWebsiteAboutUs();
    }
}
