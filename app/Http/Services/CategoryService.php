<?php

namespace App\Http\Services;

use App\Models\Category;

class CategoryService{


    public function getCategories()
    {
        return Category::get();
    }


    public function createCategory($categoryData)
    {
        $category =  Category::create($categoryData);
        return $category;
    }

    public function findCategory($id)
    {
        $category = Category::find($id);
        if(!$category) {
            throw new \Exception("Category Not Found", 404);
        }
        return $category;
    }

    public function update($data, $id)
    {
        $category = $this->findCategory($id);
        $category->update($data);
        return $category;
    }

    public function delete($id)
    {
        $category = $this->findCategory($id);
        $category->delete();
        return true;
    }
}
