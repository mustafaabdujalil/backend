<?php

namespace App\Http\Services;

use App\Models\City;

class CityService{

    public function getCities($countryId)
    {
        return City::where('country_id', $countryId)->get();
    }
}
