<?php

namespace App\Http\Services;

use App\Models\Event;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Response;

class EventService{


    public function getEvents($filter = [], $pagination = false)
    {
        $query =  Event::with('category','hostCountry','hostCity', 'gifts')
            ->when(isset($filter['key']), function ($query) use ($filter){
                $query->where('name_en', 'LIKE', '%'.$filter['key'].'%')
                    ->orWhere('name_ar', 'LIKE', '%'.$filter['key'].'%')
                    ->orWhere('description_en', 'LIKE', '%'.$filter['key'].'%')
                    ->orWhere('description_ar', 'LIKE', '%'.$filter['key'].'%');
            })
            ->when(isset($filter['category_id']), function ($query) use ($filter){
                $query->where('category_id', $filter['category_id']);
            })->when(isset($filter['type']), function ($query) use ($filter){
                $query->where('type', $filter['type']);
            });

            if($pagination){
                return $query->paginate(config('app.pagination_count'));
            }

            return $query->get();
    }


    public function createEvent($eventData)
    {
        $eventData['slug'] = SlugService::createSlug(Event::class, 'slug', request('name_en'));
        $event =  Event::create($eventData);
        $event->addMediaFromRequest('image')->toMediaCollection('image');
        return $event;
    }

    public function findEvent($id)
    {
        $event = Event::with('category','hostCountry','hostCity')
                        ->where('id', $id)->first();
        if(!$event) {
            throw new \Exception("Event Not Found", Response::HTTP_NOT_FOUND);
        }
        return $event;
    }


    public function findEventBySlug($slug)
    {
        $event = Event::with('category','hostCountry','hostCity', 'gifts')
            ->where('slug', $slug)->first();
        if(!$event) {
            throw new \Exception("Event Not Found", Response::HTTP_NOT_FOUND);
        }
        return $event;
    }

    public function update($data, $id)
    {
        $event = $this->findEvent($id);
        $data['slug'] = SlugService::createSlug(Event::class, 'slug', request('name_en'));
        Event::where('id', $id)->update($data);
        if(request()->has('image')){
            $event->clearMediaCollection('image');
            $event->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $event;
    }

    public function delete($id)
    {
        $event = $this->findEvent($id);
        $event->delete();
        return true;
    }
}
