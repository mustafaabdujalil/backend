<?php

namespace App\Http\Services;

use App\Models\Gift;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;


class GiftService{


    public function getGifts($filter = [], $list = true)
    {
        $gifts = Gift::with('event','creator','participants');
        if (isset($filter['is_gift_card'])) {
            $gifts->where('is_gift_card', $filter['is_gift_card']);
        }
        if (isset($filter['status'])) {
            $gifts->where('status', $filter['status']);
        }
        if (isset($filter['order_type']) && isset($filter['order_col'])) {
            $gifts->orderBy($filter['order_col'], $filter['order_type']);
        }
        if (isset($filter['ids'])) {
            $gifts->whereIn('id', $filter['ids']);
        }

        if($list) {
            $gifts->where('remaining','>',0)
                ->whereNotIn('id', array_column(cart()->items(),'modelId'));
        }

        return $gifts->get();
    }


    public function createGift($giftData)
    {
        $giftData['creator_id'] = Auth::user()->id;
        unset($giftData['image']);
        $gift = Gift::create($giftData);
        $gift->addMediaFromRequest('image')->toMediaCollection('image');
        return $gift;
    }

    public function findGift($id)
    {
        $gift = Gift::with('event','creator','participants')
                        ->where('id', $id)->first();
        if(!$gift) {
            throw new \Exception("Gift Not Found", Response::HTTP_NOT_FOUND);
        }
        return $gift;
    }

    public function update($data, $id)
    {
        unset($data['image']);
        Gift::where('id', $id)->update($data);
        $gift = $this->findGift($id);
        if(request()->has('image')){
            $gift->clearMediaCollection('image');
            $gift->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $gift;
    }

    public function delete($id)
    {
        $event = $this->findGift($id);
        $event->delete();
        return true;
    }
}
