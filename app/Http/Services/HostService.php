<?php

namespace App\Http\Services;

use App\Models\Host;

class HostService{


    public function getHosts($pagination = false)
    {
        if($pagination){
            return Host::query()->paginate(config('app.pagination_count'));
        }
        return Host::get();
    }


    public function createHost($hostsData)
    {
        $hosts =  Host::create($hostsData);
        return $hosts;
    }

    public function findHost($id)
    {
        $hosts = Host::find($id);
        if(!$hosts) {
            throw new \Exception("Host Not Found", 404);
        }
        return $hosts;
    }

    public function update($data, $id)
    {
        $hosts = $this->findHost($id);
        $hosts->update($data);
        return $hosts;
    }

    public function delete($id)
    {
        $hosts = $this->findHost($id);
        $hosts->delete();
        return true;
    }
}
