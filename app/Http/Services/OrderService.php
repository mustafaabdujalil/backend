<?php

namespace App\Http\Services;

use App\Models\Gift;
use App\Models\Order;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class OrderService{

    public $giftService;
    public function __construct(GiftService $giftService)
    {
        $this->giftService = $giftService;
    }


    public function getOrders($filter = [])
    {
        $orders = Order::with('items', 'items.item', 'items.event', 'client');
        if (isset($filter['client_id'])) {
            $orders->where('client_id', $filter['client_id']);
        }
        if (isset($filter['status'])) {
            $orders->where('status', $filter['status']);
        }
        if (isset($filter['is_paid'])) {
            $orders->where('is_paid', $filter['is_paid']);
        }
        if (isset($filter['ids'])) {
            $orders->whereIn('ids', $filter['ids']);
        }

        return $orders->get();
    }


    public function createOrder($request)
    {
        $orderData = [];
        $orderData['user_id'] = \auth()->id();
        $orderData['client_id'] = \auth()->id();
        $orderData['price'] = cart()->total();

        $order = Order::create($orderData);

        $this->createOrderItems($order, $request);

        return $order;
    }

    public function findOrder($id)
    {
        $order = Order::with('items','customer','client')
                        ->where('id', $id)->first();
        if(!$order) {
            throw new \Exception("Order Not Found", Response::HTTP_NOT_FOUND);
        }
        return $order;
    }

    public function createOrderItems($order, $request)
    {
        $gifts = $this->giftService->getGifts(['ids' => array_column(cart()->items(),'modelId')], false);
        $orderItems = [];
        foreach ($gifts as $gift){
            $itemIndex = $this->getItemByModelId($gift->id);
            $price = $request['price'][$itemIndex];
            $orderItems [] = [
                'order_id' => $order->id,
                'item_id' => $gift->id,
                'event_id' => $gift->event_id,
                'price' => $price,
            ];

            if($gift->is_gift_card == 0 && $gift->price > $price) {
                throw new \ErrorException(__('You need to pay full price it is not a shared gift'));
            }

            // update event paid and remaining amount
            $this->giftService->update([
                'paid' => $gift->paid + $price,
                'remaining' => $gift->remaining - $price,
                'status' => $gift->remaining <= $price ? 0 : 1
            ], $gift->id);

        }
        DB::table('order_items')->insert($orderItems);

        return $orderItems;
    }

    public function getItemByModelId($modelId)
    {
        foreach (cart()->items() as $index => $item){
            if($item['modelId'] == $modelId){
                return $index;
            }
        }
    }
}
