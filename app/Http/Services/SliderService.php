<?php

namespace App\Http\Services;

use App\Models\Slider;

class SliderService{


    public function getSliders()
    {
        return Slider::get();
    }


    public function createSlider($sliderData)
    {
        $slider =  Slider::create($sliderData);
        $slider->addMediaFromRequest('image')->toMediaCollection('image');
        return $slider;
    }

    public function findSlider($id)
    {
        $slider = Slider::find($id);
        if(!$slider) {
            throw new \Exception("Slider Not Found", 404);
        }
        return $slider;
    }

    public function update($request, $id)
    {
        $slider = $this->findSlider($id);
        $slider->update($request->all());
        if($request->image){
            $slider->clearMediaCollection('image');
            $slider->addMediaFromRequest('image')->toMediaCollection('image');
        }
        return $slider;
    }

    public function delete($id)
    {
        $slider = $this->findSlider($id);
        $slider->delete();
        return true;
    }
}
