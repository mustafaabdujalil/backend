<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class About extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'title_en',
        'title_ar',
        'content_en',
        'content_ar'
    ];

    protected $hidden = ['title_en', 'title_ar', 'content_en','content_ar','media'];

    protected $appends = ['image', 'title', 'content'];
    const MediaCollectionName = 'image';

    public function getTitleAttribute()
    {
        $currentLocal = 'title_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function getContentAttribute()
    {
        $currentLocal = 'content_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function getImageAttribute()
    {
        return $this->hasMedia(self::MediaCollectionName) ? $this->getFirstMediaUrl(self::MediaCollectionName) : config('app.default_image');
    }
}
