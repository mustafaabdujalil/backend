<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['name_en', 'name_ar', 'description_en', 'description_ar'];

    public function getNameAttribute()
    {
        $currentLocal = 'name_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function getDescriptionAttribute()
    {
        $currentLocal = 'description_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event','category_id','id');
    }

}
