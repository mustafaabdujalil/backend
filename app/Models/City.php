<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $fillable = ['name_en','name_ar','code','country_id'];

    /**
     * The attributes that are appending.
     *
     * @var array
     */
    protected $appends = ['name'];
    protected $hidden = ['name_ar','name_en'];

    /**
     * The city's name.
     *
     * @var array
     * @return
     */
    public function getNameAttribute(){
        $selectedLanguage = app()->getLocale();
        $name = 'name_'.$selectedLanguage;
        return $this->$name;
    }

    /**
     * Get  the city's country.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id','id');
    }
}
