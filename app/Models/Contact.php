<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Contact extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'contact_en',
        'contact_ar',
        'address_en',
        'address_ar',
        'email_1',
        'email_2',
        'phone_1',
        'phone_2',
        'facebook',
        'youtube',
        'twitter',
        'instagram'
    ];

    protected $appends = ['contact', 'address'];

    public function getContactAttribute()
    {
        $locale = 'contact_'.app()->getLocale();
        return $this->$locale;
    }

    public function getAddressAttribute()
    {
        $locale = 'address_'.app()->getLocale();
        return $this->$locale;
    }

}
