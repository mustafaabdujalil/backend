<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $fillable = ['name_en','name_ar','code'];

    /**
     * The attributes that are appending.
     *
     * @var array
    */
    protected $appends = ['name'];
    protected $hidden = ['name_ar','name_en'];

    /**
     * The country's name.
     *
     * @var array
     * @return
    */
    public function getNameAttribute(){
        $selectedLanguage = app()->getLocale();
        $name = 'name_'.$selectedLanguage;
        return $this->$name;
    }

    /**
     * Get all of the country's cities.
    */
    public function cities()
    {
        return $this->hasMany('App\Models\City','country_id','id');
    }
}
