<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Cviebrock\EloquentSluggable\Sluggable;


class Event extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, Sluggable;

    protected $fillable = ['name_en', 'name_ar', 'description_en', 'description_ar',
    'type', 'category_id', 'password', 'date', 'host_name', 'host_code', 'host_address',
        'host_country_id', 'host_city_id', 'slug', 'host_id'];

    const PUBLIC_EVENT = "public";
    const PRIVATE_EVENT = "private";
    const MediaCollectionName = 'image';

    protected $appends = ['image'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name_en'
            ]
        ];
    }

    public function getNameAttribute()
    {
        $currentLocal = 'name_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function getDescriptionAttribute()
    {
        $currentLocal = 'description_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function hostCountry()
    {
        return $this->belongsTo(Country::class,'host_country_id','id');
    }

    public function hostCity()
    {
        return $this->belongsTo(City::class,'host_city_id','id');
    }

    public function gifts()
    {
        return $this->hasMany(Gift::class);
    }

    public function getImageAttribute()
    {
        return $this->hasMedia(self::MediaCollectionName) ? $this->getFirstMediaUrl(self::MediaCollectionName) : asset(config('app.default_image'));
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'event_id','id');
    }

    public function orders()
    {
        return $this->hasManyThrough(Order::class, OrderItem::class,'event_id','order_id','id','id');
    }

    public function host()
    {
        return $this->belongsTo(Host::class,'host_id','id');
    }
}
