<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Freshbitsweb\LaravelCartManager\Traits\Cartable;

class Gift extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, Cartable;
    const MediaCollectionName = 'image';

    protected $fillable = [
        'name_en',
        'name_ar',
        'description_en',
        'description_ar',
        'price',
        'paid',
        'remaining',
        'status',
        'is_gift_card',
        'creator_id',
        'event_id'
    ];

    public function getNameAttribute()
    {
        $currentLocal = 'name_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function getDescriptionAttribute()
    {
        $currentLocal = 'description_'.app()->getLocale();
        return $this->$currentLocal;
    }


    public function event()
    {
        return $this->belongsTo(Event::class,'event_id','id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'creator_id','id');
    }

    public function participants()
    {
        return $this->belongsToMany(User::class, 'gift_participant')->withPivot('amount');
    }

    public function getImageAttribute()
    {
        return $this->hasMedia(self::MediaCollectionName) ? $this->getFirstMediaUrl(self::MediaCollectionName) : asset(config('app.default_image'));
    }

}
