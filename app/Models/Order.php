<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    const PENDING_STATUS = 'pending';
    const CONFIRMED_STATUS = 'confirmed';
    const CANCEL_STATUS = 'cancel';

    const PAID_STATUS = 'paid';
    const FAILED_STATUS = 'failed';

    const ORDER_STATUSES = [
        self::PENDING_STATUS,
        self::CONFIRMED_STATUS,
        self::CANCEL_STATUS
    ];

    protected $fillable = ['client_id', 'price', 'status', 'is_paid', 'payment_id',
        'payment_token', 'discount'];

    public function client()
    {
        return $this->belongsTo('App\Models\User','client_id','id');
    }

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem','order_id','id');
    }
}
