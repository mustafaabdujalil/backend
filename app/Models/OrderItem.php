<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $fillable = ['order_id', 'item_id', 'event_id', 'price', 'quantity'];

    public function item()
    {
        return $this->belongsTo('App\Models\Gift','item_id','id');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event','event_id','id');
    }
}
