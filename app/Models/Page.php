<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Page extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'name_en',
        'name_ar',
        'url',
        'details_en',
        'details_ar',
        'position'
    ];


    const HEADER_POSITION = 'header';
    const FOOTER_POSITION = 'footer';
    const BOTH_POSITION = 'both';

    protected $hidden = ['name_en', 'name_ar', 'details_en','details_ar'];

    protected $appends = ['name', 'details'];

    public function getNameAttribute()
    {
        $currentLocal = 'name_'.app()->getLocale();
        return $this->$currentLocal;
    }

    public function getDetailsAttribute()
    {
        $currentLocal = 'details_'.app()->getLocale();
        return $this->$currentLocal;
    }
}
