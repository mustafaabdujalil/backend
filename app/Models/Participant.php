<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use HasFactory;
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'country_id', 'city_id'];

    // return country
    public function country(){
        return $this->belongsTo('App\Models\Country','country_id','id');
    }

    // return city
    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id');
    }
}
