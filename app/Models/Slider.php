<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Slider extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    const MediaCollectionName = 'image';

    protected $fillable = [
        'title_en',
        'title_ar',
        'content_en',
        'content_ar',
        'url'
    ];

    protected $appends = ['image', 'title', 'content'];

    public function getTitleAttribute()
    {
        $locale = 'title_'.app()->getLocale();
        return $this->$locale;
    }

    public function getContentAttribute()
    {
        $locale = 'content_'.app()->getLocale();
        return $this->$locale;
    }

    public function getImageAttribute()
    {
        return $this->hasMedia(self::MediaCollectionName) ? $this->getFirstMediaUrl(self::MediaCollectionName) : asset(config('app.default_image'));
    }
}
