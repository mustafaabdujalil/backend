<?php

namespace App\Providers;

use App\Constants\Constants;
use App\Models\About;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Event;
use App\Models\Gift;
use App\Models\Page;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $pages = Cache::remember('pages', '1000', function (){
           return Page::all();
        });
        $about = Cache::remember('about', '1000', function (){
            return About::query()->first();
        });

        $countries = Cache::remember('countries', '1000', function (){
            return Country::all();
        });

        $contact = Cache::remember('contact', '1000', function (){
            return Contact::query()->first();
        });

        $completedGifts = Cache::remember('completeGifts', '1000', function (){
            return Gift::where('status', '!=', Constants::INACTIVE_STATUS)->inRandomOrder()->take(5)->get();
        });

        $latestEvents = Cache::remember('latestEvents', '1000', function (){
            return Event::where('date', '>=', now())->inRandomOrder()->take(3)->get();
        });

        $eventsCategories = Cache::remember('eventsCategories', '1000', function (){
            return Category::whereHas('events')->get();
        });

        view()->share([
            'pages' => $pages,
            'contact' => $contact,
            'completedGifts' => $completedGifts,
            'latestEvents' => $latestEvents,
            'eventsCategories' => $eventsCategories,
            'countries' => $countries,
            'about' => $about
        ]);
    }
}
