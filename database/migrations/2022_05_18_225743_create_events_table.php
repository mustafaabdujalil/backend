<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name_en');
            $table->string('name_ar')->nullable();
            $table->text('description_en');
            $table->text('description_ar')->nullable();
            $table->string('password')->nullable();
            $table->date('date');
            $table->unsignedBigInteger('category_id');
            $table->string('type')->default('public')->comment('public,private');
            $table->boolean('is_gift_card_allowed')->default(false);

            $table->string('host_name')->nullable();
            $table->string('host_code')->nullable();
            $table->unsignedBigInteger('host_country_id')->nullable();
            $table->unsignedBigInteger('host_city_id')->nullable();
            $table->string('host_address')->nullable();

            $table->foreign('category_id')->references('id')->on('categories')->cascadeOnDelete();
            $table->foreign('host_country_id')->references('id')->on('countries')->cascadeOnDelete();
            $table->foreign('host_city_id')->references('id')->on('cities')->cascadeOnDelete();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
