<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->id();
            $table->string('name_en');
            $table->string('name_ar')->nullable();
            $table->text('description_en');
            $table->text('description_ar')->nullable();
            $table->float('price');
            $table->integer('status')->default(1)->comment('0:not_available, 1:available');
            $table->boolean('is_gift_card')->default(false);
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('event_id');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('event_id')->references('id')->on('events');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
