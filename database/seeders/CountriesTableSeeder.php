<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        // truncate countries table
        Country::query()->truncate();
        Schema::enableForeignKeyConstraints();

        $countries = array(
            array('id' => '1','name' => 'أندورا','name_fr' => 'Andorre','name_en' => 'Andorra','code' => 'ad','created_at' => NULL,'updated_at' => NULL),
            array('id' => '2','name' => 'الإمارات العربية المتحدة','name_fr' => 'Emirats Arabes Unis','name_en' => 'United Arab Emirates','code' => 'ae','created_at' => NULL,'updated_at' => NULL),
            array('id' => '3','name' => 'أفغانستان','name_fr' => 'L\'Afghanistan','name_en' => 'Afghanistan','code' => 'af','created_at' => NULL,'updated_at' => NULL),
            array('id' => '4','name' => 'أنتيغوا وبربودا','name_fr' => 'Antigua-et-Barbuda','name_en' => 'Antigua and Barbuda','code' => 'ag','created_at' => NULL,'updated_at' => NULL),
            array('id' => '5','name' => 'أنغيلا','name_fr' => 'Anguilla','name_en' => 'Anguilla','code' => 'ai','created_at' => NULL,'updated_at' => NULL),
            array('id' => '6','name' => 'ألبانيا','name_fr' => 'Albanie','name_en' => 'Albania','code' => 'al','created_at' => NULL,'updated_at' => NULL),
            array('id' => '7','name' => 'أرمينيا','name_fr' => 'Arménie','name_en' => 'Armenia','code' => 'am','created_at' => NULL,'updated_at' => NULL),
            array('id' => '8','name' => 'جزر الأنتيل الهولندية','name_fr' => 'Antilles néerlandaises','name_en' => 'Netherlands Antilles','code' => 'an','created_at' => NULL,'updated_at' => NULL),
            array('id' => '9','name' => 'أنغولا','name_fr' => 'Angola','name_en' => 'Angola','code' => 'ao','created_at' => NULL,'updated_at' => NULL),
            array('id' => '10','name' => 'الأرجنتين','name_fr' => 'Argentine','name_en' => 'Argentina','code' => 'ar','created_at' => NULL,'updated_at' => NULL),
            array('id' => '11','name' => 'النمسا','name_fr' => 'L\'Autriche','name_en' => 'Austria','code' => 'at','created_at' => NULL,'updated_at' => NULL),
            array('id' => '12','name' => 'أستراليا','name_fr' => 'Australie','name_en' => 'Australia','code' => 'au','created_at' => NULL,'updated_at' => NULL),
            array('id' => '13','name' => 'أروبا','name_fr' => 'Aruba','name_en' => 'Aruba','code' => 'aw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '14','name' => 'أذربيجان','name_fr' => 'Azerbaïdjan','name_en' => 'Azerbaijan','code' => 'az','created_at' => NULL,'updated_at' => NULL),
            array('id' => '15','name' => 'البوسنة والهرسك','name_fr' => 'Bosnie Herzégovine','name_en' => 'Bosnia and Herzegovina','code' => 'ba','created_at' => NULL,'updated_at' => NULL),
            array('id' => '16','name' => 'بربادوس','name_fr' => 'La Barbade','name_en' => 'Barbados','code' => 'bb','created_at' => NULL,'updated_at' => NULL),
            array('id' => '17','name' => 'بنغلاديش','name_fr' => 'Bangladesh','name_en' => 'Bangladesh','code' => 'bd','created_at' => NULL,'updated_at' => NULL),
            array('id' => '18','name' => 'بلجيكا','name_fr' => 'Belgique','name_en' => 'Belgium','code' => 'be','created_at' => NULL,'updated_at' => NULL),
            array('id' => '19','name' => 'بوركينا فاسو','name_fr' => 'Burkina Faso','name_en' => 'Burkina Faso','code' => 'bf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '20','name' => 'بلغاريا','name_fr' => 'Bulgarie','name_en' => 'Bulgaria','code' => 'bg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '21','name' => 'البحرين','name_fr' => 'Bahreïn','name_en' => 'Bahrain','code' => 'bh','created_at' => NULL,'updated_at' => NULL),
            array('id' => '22','name' => 'بوروندي','name_fr' => 'Burundi','name_en' => 'Burundi','code' => 'bi','created_at' => NULL,'updated_at' => NULL),
            array('id' => '23','name' => 'بنين','name_fr' => 'Bénin','name_en' => 'Benin','code' => 'bj','created_at' => NULL,'updated_at' => NULL),
            array('id' => '24','name' => 'برمودا','name_fr' => 'Bermudes','name_en' => 'Bermuda','code' => 'bm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '25','name' => 'بروناي دار السلام','name_fr' => 'Brunei Darussalam','name_en' => 'Brunei Darussalam','code' => 'bn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '26','name' => 'بوليفيا','name_fr' => 'Bolivie','name_en' => 'Bolivia','code' => 'bo','created_at' => NULL,'updated_at' => NULL),
            array('id' => '27','name' => 'البرازيل','name_fr' => 'Brésil','name_en' => 'Brazil','code' => 'br','created_at' => NULL,'updated_at' => NULL),
            array('id' => '28','name' => 'الباهاما','name_fr' => 'Bahamas','name_en' => 'Bahamas','code' => 'bs','created_at' => NULL,'updated_at' => NULL),
            array('id' => '29','name' => 'بوتان','name_fr' => 'Bhoutan','name_en' => 'Bhutan','code' => 'bt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '30','name' => 'بوتسوانا','name_fr' => 'Botswana','name_en' => 'Botswana','code' => 'bw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '31','name' => 'روسيا البيضاء','name_fr' => 'Biélorussie','name_en' => 'Belarus','code' => 'by','created_at' => NULL,'updated_at' => NULL),
            array('id' => '32','name' => 'بليز','name_fr' => 'Belize','name_en' => 'Belize','code' => 'bz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '33','name' => 'كندا','name_fr' => 'Canada','name_en' => 'Canada','code' => 'ca','created_at' => NULL,'updated_at' => NULL),
            array('id' => '34','name' => 'جزر كوكوس (كيلينغ)','name_fr' => 'Îles Cocos (Keeling)','name_en' => 'Cocos (Keeling) Islands','code' => 'cc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '35','name' => 'جمهورية الكونغو الديموقراطية','name_fr' => 'République Démocratique du Congo','name_en' => 'Democratic Republic of the Congo','code' => 'cd','created_at' => NULL,'updated_at' => NULL),
            array('id' => '36','name' => 'جمهورية افريقيا الوسطى','name_fr' => 'République centrafricaine','name_en' => 'Central African Republic','code' => 'cf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '37','name' => 'الكونغو','name_fr' => 'Congo','name_en' => 'Congo','code' => 'cg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '38','name' => 'سويسرا','name_fr' => 'Suisse','name_en' => 'Switzerland','code' => 'ch','created_at' => NULL,'updated_at' => NULL),
            array('id' => '39','name' => 'ساحل العاج (ساحل العاج)','name_fr' => 'Cote D\'Ivoire (Côte d\'Ivoire)','name_en' => 'Cote D\'Ivoire (Ivory Coast)','code' => 'ci','created_at' => NULL,'updated_at' => NULL),
            array('id' => '40','name' => 'جزر كوك','name_fr' => 'les Îles Cook','name_en' => 'Cook Islands','code' => 'ck','created_at' => NULL,'updated_at' => NULL),
            array('id' => '41','name' => 'تشيلي','name_fr' => 'Chili','name_en' => 'Chile','code' => 'cl','created_at' => NULL,'updated_at' => NULL),
            array('id' => '42','name' => 'الكاميرون','name_fr' => 'Cameroun','name_en' => 'Cameroon','code' => 'cm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '43','name' => 'الصين','name_fr' => 'Chine','name_en' => 'China','code' => 'cn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '44','name' => 'كولومبيا','name_fr' => 'Colombie','name_en' => 'Colombia','code' => 'co','created_at' => NULL,'updated_at' => NULL),
            array('id' => '45','name' => 'كوستا ريكا','name_fr' => 'Costa Rica','name_en' => 'Costa Rica','code' => 'cr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '46','name' => 'كوبا','name_fr' => 'Cuba','name_en' => 'Cuba','code' => 'cu','created_at' => NULL,'updated_at' => NULL),
            array('id' => '47','name' => 'الرأس الأخضر','name_fr' => 'Cap-Vert','name_en' => 'Cape Verde','code' => 'cv','created_at' => NULL,'updated_at' => NULL),
            array('id' => '48','name' => 'جزيرة الكريسماس','name_fr' => 'L\'île de noël','name_en' => 'Christmas Island','code' => 'cx','created_at' => NULL,'updated_at' => NULL),
            array('id' => '49','name' => 'قبرص','name_fr' => 'Chypre','name_en' => 'Cyprus','code' => 'cy','created_at' => NULL,'updated_at' => NULL),
            array('id' => '50','name' => 'جمهورية التشيك','name_fr' => 'République Tchèque','name_en' => 'Czech Republic','code' => 'cz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '51','name' => 'ألمانيا','name_fr' => 'Allemagne','name_en' => 'Germany','code' => 'de','created_at' => NULL,'updated_at' => NULL),
            array('id' => '52','name' => 'جيبوتي','name_fr' => 'Djibouti','name_en' => 'Djibouti','code' => 'dj','created_at' => NULL,'updated_at' => NULL),
            array('id' => '53','name' => 'الدنمارك','name_fr' => 'Danemark','name_en' => 'Denmark','code' => 'dk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '54','name' => 'دومينيكا','name_fr' => 'Dominique','name_en' => 'Dominica','code' => 'dm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '55','name' => 'جمهورية الدومنيكان','name_fr' => 'République Dominicaine','name_en' => 'Dominican Republic','code' => 'do','created_at' => NULL,'updated_at' => NULL),
            array('id' => '56','name' => 'الجزائر','name_fr' => 'Algérie','name_en' => 'Algeria','code' => 'dz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '57','name' => 'الإكوادور','name_fr' => 'L\'Équateur','name_en' => 'Ecuador','code' => 'ec','created_at' => NULL,'updated_at' => NULL),
            array('id' => '58','name' => 'استونيا','name_fr' => 'Estonie','name_en' => 'Estonia','code' => 'ee','created_at' => NULL,'updated_at' => NULL),
            array('id' => '59','name' => 'مصر','name_fr' => 'Egypte','name_en' => 'Egypt','code' => 'eg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '60','name' => 'الصحراء الغربية','name_fr' => 'Sahara occidental','name_en' => 'Western Sahara','code' => 'eh','created_at' => NULL,'updated_at' => NULL),
            array('id' => '61','name' => 'إريتريا','name_fr' => 'Erythrée','name_en' => 'Eritrea','code' => 'er','created_at' => NULL,'updated_at' => NULL),
            array('id' => '62','name' => 'إسبانيا','name_fr' => 'Espagne','name_en' => 'Spain','code' => 'es','created_at' => NULL,'updated_at' => NULL),
            array('id' => '63','name' => 'أثيوبيا','name_fr' => 'Ethiopie','name_en' => 'Ethiopia','code' => 'et','created_at' => NULL,'updated_at' => NULL),
            array('id' => '64','name' => 'فنلندا','name_fr' => 'Finlande','name_en' => 'Finland','code' => 'fi','created_at' => NULL,'updated_at' => NULL),
            array('id' => '65','name' => 'فيجي','name_fr' => 'Fidji','name_en' => 'Fiji','code' => 'fj','created_at' => NULL,'updated_at' => NULL),
            array('id' => '66','name' => 'جزر فوكلاند (مالفيناس)','name_fr' => 'Iles Malouines (Malouines)','name_en' => 'Falkland Islands (Malvinas)','code' => 'fk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '67','name' => 'ولايات ميكرونيزيا الموحدة','name_fr' => 'États fédérés de Micronésie','name_en' => 'Federated States of Micronesia','code' => 'fm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '68','name' => 'جزر صناعية','name_fr' => 'Îles Féroé','name_en' => 'Faroe Islands','code' => 'fo','created_at' => NULL,'updated_at' => NULL),
            array('id' => '69','name' => 'فرنسا','name_fr' => 'France','name_en' => 'France','code' => 'fr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '70','name' => 'الغابون','name_fr' => 'Gabon','name_en' => 'Gabon','code' => 'ga','created_at' => NULL,'updated_at' => NULL),
            array('id' => '71','name' => 'بريطانيا العظمى (المملكة المتحدة)','name_fr' => 'Grande-Bretagne (UK)','name_en' => 'Great Britain (UK)','code' => 'gb','created_at' => NULL,'updated_at' => NULL),
            array('id' => '72','name' => 'غرينادا','name_fr' => 'Grenade','name_en' => 'Grenada','code' => 'gd','created_at' => NULL,'updated_at' => NULL),
            array('id' => '73','name' => 'جورجيا','name_fr' => 'Géorgie','name_en' => 'Georgia','code' => 'ge','created_at' => NULL,'updated_at' => NULL),
            array('id' => '74','name' => 'غيانا الفرنسية','name_fr' => 'Guinée Française','name_en' => 'French Guiana','code' => 'gf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '75','name' => 'لا شيء','name_fr' => 'NUL','name_en' => 'NULL','code' => 'gg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '76','name' => 'غانا','name_fr' => 'Ghana','name_en' => 'Ghana','code' => 'gh','created_at' => NULL,'updated_at' => NULL),
            array('id' => '77','name' => 'جبل طارق','name_fr' => 'Gibraltar','name_en' => 'Gibraltar','code' => 'gi','created_at' => NULL,'updated_at' => NULL),
            array('id' => '78','name' => 'الأرض الخضراء','name_fr' => 'Groenland','name_en' => 'Greenland','code' => 'gl','created_at' => NULL,'updated_at' => NULL),
            array('id' => '79','name' => 'غامبيا','name_fr' => 'Gambie','name_en' => 'Gambia','code' => 'gm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '80','name' => 'غينيا','name_fr' => 'Guinée','name_en' => 'Guinea','code' => 'gn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '81','name' => 'جوادلوب','name_fr' => 'La guadeloupe','name_en' => 'Guadeloupe','code' => 'gp','created_at' => NULL,'updated_at' => NULL),
            array('id' => '82','name' => 'غينيا الإستوائية','name_fr' => 'Guinée Équatoriale','name_en' => 'Equatorial Guinea','code' => 'gq','created_at' => NULL,'updated_at' => NULL),
            array('id' => '83','name' => 'اليونان','name_fr' => 'Grèce','name_en' => 'Greece','code' => 'gr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '84','name' => 'جورجيا وجزر ساندويتش','name_fr' => 'Géorgie du Sud et les îles Sandwich du Sud','name_en' => 'S. Georgia and S. Sandwich Islands','code' => 'gs','created_at' => NULL,'updated_at' => NULL),
            array('id' => '85','name' => 'غواتيمالا','name_fr' => 'Guatemala','name_en' => 'Guatemala','code' => 'gt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '86','name' => 'غينيا بيساو','name_fr' => 'Guinée-Bissau','name_en' => 'Guinea-Bissau','code' => 'gw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '87','name' => 'غيانا','name_fr' => 'Guyane','name_en' => 'Guyana','code' => 'gy','created_at' => NULL,'updated_at' => NULL),
            array('id' => '88','name' => 'هونغ كونغ','name_fr' => 'Hong Kong','name_en' => 'Hong Kong','code' => 'hk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '89','name' => 'هندوراس','name_fr' => 'Honduras','name_en' => 'Honduras','code' => 'hn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '90','name' => 'كرواتيا (هرفاتسكا)','name_fr' => 'Croatie (Hrvatska)','name_en' => 'Croatia (Hrvatska)','code' => 'hr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '91','name' => 'هايتي','name_fr' => 'Haïti','name_en' => 'Haiti','code' => 'ht','created_at' => NULL,'updated_at' => NULL),
            array('id' => '92','name' => 'اليونان','name_fr' => 'Hongrie','name_en' => 'Hungary','code' => 'hu','created_at' => NULL,'updated_at' => NULL),
            array('id' => '93','name' => 'أندونيسيا','name_fr' => 'Indonésie','name_en' => 'Indonesia','code' => 'id','created_at' => NULL,'updated_at' => NULL),
            array('id' => '94','name' => 'أيرلندا','name_fr' => 'Irlande','name_en' => 'Ireland','code' => 'ie','created_at' => NULL,'updated_at' => NULL),
            array('id' => '96','name' => 'الهند','name_fr' => 'Inde','name_en' => 'India','code' => 'in','created_at' => NULL,'updated_at' => NULL),
            array('id' => '97','name' => 'العراق','name_fr' => 'Irak','name_en' => 'Iraq','code' => 'iq','created_at' => NULL,'updated_at' => NULL),
            array('id' => '98','name' => 'إيران','name_fr' => 'Iran','name_en' => 'Iran','code' => 'ir','created_at' => NULL,'updated_at' => NULL),
            array('id' => '99','name' => 'أيسلندا','name_fr' => 'Islande','name_en' => 'Iceland','code' => 'is','created_at' => NULL,'updated_at' => NULL),
            array('id' => '100','name' => 'إيطاليا','name_fr' => 'Italie','name_en' => 'Italy','code' => 'it','created_at' => NULL,'updated_at' => NULL),
            array('id' => '101','name' => 'جامايكا','name_fr' => 'Jamaïque','name_en' => 'Jamaica','code' => 'jm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '102','name' => 'الأردن','name_fr' => 'Jordan','name_en' => 'Jordan','code' => 'jo','created_at' => NULL,'updated_at' => NULL),
            array('id' => '103','name' => 'اليابان','name_fr' => 'Japon','name_en' => 'Japan','code' => 'jp','created_at' => NULL,'updated_at' => NULL),
            array('id' => '104','name' => 'كينيا','name_fr' => 'Kenya','name_en' => 'Kenya','code' => 'ke','created_at' => NULL,'updated_at' => NULL),
            array('id' => '105','name' => 'قرغيزستان','name_fr' => 'Kirghizistan','name_en' => 'Kyrgyzstan','code' => 'kg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '106','name' => 'كمبوديا','name_fr' => 'Cambodge','name_en' => 'Cambodia','code' => 'kh','created_at' => NULL,'updated_at' => NULL),
            array('id' => '107','name' => 'كيريباس','name_fr' => 'Kiribati','name_en' => 'Kiribati','code' => 'ki','created_at' => NULL,'updated_at' => NULL),
            array('id' => '108','name' => 'جزر القمر','name_fr' => 'Comores','name_en' => 'Comoros','code' => 'km','created_at' => NULL,'updated_at' => NULL),
            array('id' => '109','name' => 'سانت كيتس ونيفيس','name_fr' => 'Saint-Christophe-et-Niévès','name_en' => 'Saint Kitts and Nevis','code' => 'kn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '110','name' => 'كوريا الشمالية','name_fr' => 'Corée du Nord','name_en' => 'Korea (North)','code' => 'kp','created_at' => NULL,'updated_at' => NULL),
            array('id' => '111','name' => 'كوريا، جنوب)','name_fr' => 'COREE DU SUD)','name_en' => 'Korea (South)','code' => 'kr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '112','name' => 'الكويت','name_fr' => 'Koweit','name_en' => 'Kuwait','code' => 'kw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '113','name' => 'جزر كايمان','name_fr' => 'Îles Caïmans','name_en' => 'Cayman Islands','code' => 'ky','created_at' => NULL,'updated_at' => NULL),
            array('id' => '114','name' => 'كازاخستان','name_fr' => 'Le kazakhstan','name_en' => 'Kazakhstan','code' => 'kz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '115','name' => 'لاوس','name_fr' => 'Laos','name_en' => 'Laos','code' => 'la','created_at' => NULL,'updated_at' => NULL),
            array('id' => '116','name' => 'لبنان','name_fr' => 'Liban','name_en' => 'Lebanon','code' => 'lb','created_at' => NULL,'updated_at' => NULL),
            array('id' => '117','name' => 'القديسة لوسيا','name_fr' => 'Sainte-Lucie','name_en' => 'Saint Lucia','code' => 'lc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '118','name' => 'ليختنشتاين','name_fr' => 'Le Liechtenstein','name_en' => 'Liechtenstein','code' => 'li','created_at' => NULL,'updated_at' => NULL),
            array('id' => '119','name' => 'سيريلانكا','name_fr' => 'Sri Lanka','name_en' => 'Sri Lanka','code' => 'lk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '120','name' => 'ليبيريا','name_fr' => 'Libéria','name_en' => 'Liberia','code' => 'lr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '121','name' => 'ليسوتو','name_fr' => 'Lesotho','name_en' => 'Lesotho','code' => 'ls','created_at' => NULL,'updated_at' => NULL),
            array('id' => '122','name' => 'ليتوانيا','name_fr' => 'Lituanie','name_en' => 'Lithuania','code' => 'lt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '123','name' => 'لوكسمبورغ','name_fr' => 'Luxembourg','name_en' => 'Luxembourg','code' => 'lu','created_at' => NULL,'updated_at' => NULL),
            array('id' => '124','name' => 'لاتفيا','name_fr' => 'Lettonie','name_en' => 'Latvia','code' => 'lv','created_at' => NULL,'updated_at' => NULL),
            array('id' => '125','name' => 'ليبيا','name_fr' => 'Libye','name_en' => 'Libya','code' => 'ly','created_at' => NULL,'updated_at' => NULL),
            array('id' => '126','name' => 'المغرب','name_fr' => 'Maroc','name_en' => 'Morocco','code' => 'ma','created_at' => NULL,'updated_at' => NULL),
            array('id' => '127','name' => 'موناكو','name_fr' => 'Monaco','name_en' => 'Monaco','code' => 'mc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '128','name' => 'مولدوفا','name_fr' => 'La Moldavie','name_en' => 'Moldova','code' => 'md','created_at' => NULL,'updated_at' => NULL),
            array('id' => '129','name' => 'مدغشقر','name_fr' => 'Madagascar','name_en' => 'Madagascar','code' => 'mg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '130','name' => 'جزر مارشال','name_fr' => 'Iles Marshall','name_en' => 'Marshall Islands','code' => 'mh','created_at' => NULL,'updated_at' => NULL),
            array('id' => '131','name' => 'مقدونيا','name_fr' => 'Macédoine','name_en' => 'Macedonia','code' => 'mk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '132','name' => 'مالي','name_fr' => 'Mali','name_en' => 'Mali','code' => 'ml','created_at' => NULL,'updated_at' => NULL),
            array('id' => '133','name' => 'ميانمار','name_fr' => 'Myanmar','name_en' => 'Myanmar','code' => 'mm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '134','name' => 'منغوليا','name_fr' => 'Mongolie','name_en' => 'Mongolia','code' => 'mn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '135','name' => 'ماكاو','name_fr' => 'Macao','name_en' => 'Macao','code' => 'mo','created_at' => NULL,'updated_at' => NULL),
            array('id' => '136','name' => 'جزر مريانا الشمالية','name_fr' => 'Îles Mariannes du Nord','name_en' => 'Northern Mariana Islands','code' => 'mp','created_at' => NULL,'updated_at' => NULL),
            array('id' => '137','name' => 'مارتينيك','name_fr' => 'Martinique','name_en' => 'Martinique','code' => 'mq','created_at' => NULL,'updated_at' => NULL),
            array('id' => '138','name' => 'موريتانيا','name_fr' => 'Mauritanie','name_en' => 'Mauritania','code' => 'mr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '139','name' => 'مونتسيرات','name_fr' => 'Montserrat','name_en' => 'Montserrat','code' => 'ms','created_at' => NULL,'updated_at' => NULL),
            array('id' => '140','name' => 'مالطا','name_fr' => 'Malte','name_en' => 'Malta','code' => 'mt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '141','name' => 'موريشيوس','name_fr' => 'Maurice','name_en' => 'Mauritius','code' => 'mu','created_at' => NULL,'updated_at' => NULL),
            array('id' => '142','name' => 'جزر المالديف','name_fr' => 'Maldives','name_en' => 'Maldives','code' => 'mv','created_at' => NULL,'updated_at' => NULL),
            array('id' => '143','name' => 'مالاوي','name_fr' => 'Malawi','name_en' => 'Malawi','code' => 'mw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '144','name' => 'المكسيك','name_fr' => 'Mexique','name_en' => 'Mexico','code' => 'mx','created_at' => NULL,'updated_at' => NULL),
            array('id' => '145','name' => 'ماليزيا','name_fr' => 'Malaisie','name_en' => 'Malaysia','code' => 'my','created_at' => NULL,'updated_at' => NULL),
            array('id' => '146','name' => 'موزمبيق','name_fr' => 'Mozambique','name_en' => 'Mozambique','code' => 'mz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '147','name' => 'ناميبيا','name_fr' => 'Namibie','name_en' => 'Namibia','code' => 'na','created_at' => NULL,'updated_at' => NULL),
            array('id' => '148','name' => 'كاليدونيا الجديدة','name_fr' => 'Nouvelle Calédonie','name_en' => 'New Caledonia','code' => 'nc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '149','name' => 'النيجر','name_fr' => 'Niger','name_en' => 'Niger','code' => 'ne','created_at' => NULL,'updated_at' => NULL),
            array('id' => '150','name' => 'جزيرة نورفولك','name_fr' => 'l\'ile de Norfolk','name_en' => 'Norfolk Island','code' => 'nf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '151','name' => 'نيجيريا','name_fr' => 'Nigeria','name_en' => 'Nigeria','code' => 'ng','created_at' => NULL,'updated_at' => NULL),
            array('id' => '152','name' => 'نيكاراغوا','name_fr' => 'Nicaragua','name_en' => 'Nicaragua','code' => 'ni','created_at' => NULL,'updated_at' => NULL),
            array('id' => '153','name' => 'هولندا','name_fr' => 'Pays-Bas','name_en' => 'Netherlands','code' => 'nl','created_at' => NULL,'updated_at' => NULL),
            array('id' => '154','name' => 'النرويج','name_fr' => 'Norvège','name_en' => 'Norway','code' => 'no','created_at' => NULL,'updated_at' => NULL),
            array('id' => '155','name' => 'نيبال','name_fr' => 'Népal','name_en' => 'Nepal','code' => 'np','created_at' => NULL,'updated_at' => NULL),
            array('id' => '156','name' => 'ناورو','name_fr' => 'Nauru','name_en' => 'Nauru','code' => 'nr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '157','name' => 'نيوي','name_fr' => 'Niue','name_en' => 'Niue','code' => 'nu','created_at' => NULL,'updated_at' => NULL),
            array('id' => '158','name' => 'نيوزيلندا (اوتياروا)','name_fr' => 'Nouvelle-Zélande (Aotearoa)','name_en' => 'New Zealand (Aotearoa)','code' => 'nz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '159','name' => 'سلطنة عمان','name_fr' => 'Oman','name_en' => 'Oman','code' => 'om','created_at' => NULL,'updated_at' => NULL),
            array('id' => '160','name' => 'بناما','name_fr' => 'Panama','name_en' => 'Panama','code' => 'pa','created_at' => NULL,'updated_at' => NULL),
            array('id' => '161','name' => 'بيرو','name_fr' => 'Pérou','name_en' => 'Peru','code' => 'pe','created_at' => NULL,'updated_at' => NULL),
            array('id' => '162','name' => 'بولينيزيا الفرنسية','name_fr' => 'Polynésie française','name_en' => 'French Polynesia','code' => 'pf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '163','name' => 'بابوا غينيا الجديدة','name_fr' => 'Papouasie Nouvelle Guinée','name_en' => 'Papua New Guinea','code' => 'pg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '164','name' => 'الفلبين','name_fr' => 'Philippines','name_en' => 'Philippines','code' => 'ph','created_at' => NULL,'updated_at' => NULL),
            array('id' => '165','name' => 'باكستان','name_fr' => 'Pakistan','name_en' => 'Pakistan','code' => 'pk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '166','name' => 'بولندا','name_fr' => 'Pologne','name_en' => 'Poland','code' => 'pl','created_at' => NULL,'updated_at' => NULL),
            array('id' => '167','name' => 'سانت بيير وميكلون','name_fr' => 'Saint Pierre et Miquelon','name_en' => 'Saint Pierre and Miquelon','code' => 'pm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '168','name' => 'بيتكيرن','name_fr' => 'Pitcairn','name_en' => 'Pitcairn','code' => 'pn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '169','name' => 'الأراضي الفلسطينية','name_fr' => 'Territoire Palestinien','name_en' => 'Palestinian Territory','code' => 'ps','created_at' => NULL,'updated_at' => NULL),
            array('id' => '170','name' => 'البرتغال','name_fr' => 'le Portugal','name_en' => 'Portugal','code' => 'pt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '171','name' => 'بالاو','name_fr' => 'Palau','name_en' => 'Palau','code' => 'pw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '172','name' => 'باراغواي','name_fr' => 'Paraguay','name_en' => 'Paraguay','code' => 'py','created_at' => NULL,'updated_at' => NULL),
            array('id' => '173','name' => 'دولة قطر','name_fr' => 'Qatar','name_en' => 'Qatar','code' => 'qa','created_at' => NULL,'updated_at' => NULL),
            array('id' => '174','name' => 'جمع شمل','name_fr' => 'Réunion','name_en' => 'Reunion','code' => 're','created_at' => NULL,'updated_at' => NULL),
            array('id' => '175','name' => 'رومانيا','name_fr' => 'Roumanie','name_en' => 'Romania','code' => 'ro','created_at' => NULL,'updated_at' => NULL),
            array('id' => '176','name' => 'الاتحاد الروسي','name_fr' => 'Fédération Russe','name_en' => 'Russian Federation','code' => 'ru','created_at' => NULL,'updated_at' => NULL),
            array('id' => '177','name' => 'رواندا','name_fr' => 'Rwanda','name_en' => 'Rwanda','code' => 'rw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '178','name' => 'المملكة العربية السعودية','name_fr' => 'Arabie Saoudite','name_en' => 'Saudi Arabia','code' => 'sa','created_at' => NULL,'updated_at' => NULL),
            array('id' => '179','name' => 'جزر سليمان','name_fr' => 'Les îles Salomon','name_en' => 'Solomon Islands','code' => 'sb','created_at' => NULL,'updated_at' => NULL),
            array('id' => '180','name' => 'سيشيل','name_fr' => 'les Seychelles','name_en' => 'Seychelles','code' => 'sc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '181','name' => 'سودان','name_fr' => 'Soudan','name_en' => 'Sudan','code' => 'sd','created_at' => NULL,'updated_at' => NULL),
            array('id' => '182','name' => 'السويد','name_fr' => 'Suède','name_en' => 'Sweden','code' => 'se','created_at' => NULL,'updated_at' => NULL),
            array('id' => '183','name' => 'سنغافورة','name_fr' => 'Singapour','name_en' => 'Singapore','code' => 'sg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '184','name' => 'سانت هيلانة','name_fr' => 'Sainte Hélène','name_en' => 'Saint Helena','code' => 'sh','created_at' => NULL,'updated_at' => NULL),
            array('id' => '185','name' => 'سلوفينيا','name_fr' => 'La slovénie','name_en' => 'Slovenia','code' => 'si','created_at' => NULL,'updated_at' => NULL),
            array('id' => '186','name' => 'سفالبارد وجان مايان','name_fr' => 'Svalbard et Jan Mayen','name_en' => 'Svalbard and Jan Mayen','code' => 'sj','created_at' => NULL,'updated_at' => NULL),
            array('id' => '187','name' => 'سلوفاكيا','name_fr' => 'La slovaquie','name_en' => 'Slovakia','code' => 'sk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '188','name' => 'سيرا ليون','name_fr' => 'Sierra Leone','name_en' => 'Sierra Leone','code' => 'sl','created_at' => NULL,'updated_at' => NULL),
            array('id' => '189','name' => 'سان مارينو','name_fr' => 'Saint Marin','name_en' => 'San Marino','code' => 'sm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '190','name' => 'السنغال','name_fr' => 'Sénégal','name_en' => 'Senegal','code' => 'sn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '191','name' => 'الصومال','name_fr' => 'Somalie','name_en' => 'Somalia','code' => 'so','created_at' => NULL,'updated_at' => NULL),
            array('id' => '192','name' => 'سورينام','name_fr' => 'Suriname','name_en' => 'Suriname','code' => 'sr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '193','name' => 'ساو تومي وبرنسيبي','name_fr' => 'Sao Tomé et Principe','name_en' => 'Sao Tome and Principe','code' => 'st','created_at' => NULL,'updated_at' => NULL),
            array('id' => '194','name' => 'السلفادور','name_fr' => 'Le Salvador','name_en' => 'El Salvador','code' => 'sv','created_at' => NULL,'updated_at' => NULL),
            array('id' => '195','name' => 'سوريا','name_fr' => 'Syrie','name_en' => 'Syria','code' => 'sy','created_at' => NULL,'updated_at' => NULL),
            array('id' => '196','name' => 'سوازيلاند','name_fr' => 'Swaziland','name_en' => 'Swaziland','code' => 'sz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '197','name' => 'جزر تركس وكايكوس','name_fr' => 'îles Turques-et-Caïques','name_en' => 'Turks and Caicos Islands','code' => 'tc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '198','name' => 'تشاد','name_fr' => 'Le tchad','name_en' => 'Chad','code' => 'td','created_at' => NULL,'updated_at' => NULL),
            array('id' => '199','name' => 'المناطق الجنوبية لفرنسا','name_fr' => 'Terres australes françaises','name_en' => 'French Southern Territories','code' => 'tf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '200','name' => 'ليذهب','name_fr' => 'Aller','name_en' => 'Togo','code' => 'tg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '201','name' => 'تايلاند','name_fr' => 'Thaïlande','name_en' => 'Thailand','code' => 'th','created_at' => NULL,'updated_at' => NULL),
            array('id' => '202','name' => 'طاجيكستان','name_fr' => 'Tadjikistan','name_en' => 'Tajikistan','code' => 'tj','created_at' => NULL,'updated_at' => NULL),
            array('id' => '203','name' => 'توكيلاو','name_fr' => 'Tokelau','name_en' => 'Tokelau','code' => 'tk','created_at' => NULL,'updated_at' => NULL),
            array('id' => '204','name' => 'تركمانستان','name_fr' => 'Turkménistan','name_en' => 'Turkmenistan','code' => 'tm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '205','name' => 'تونس','name_fr' => 'Tunisie','name_en' => 'Tunisia','code' => 'tn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '206','name' => 'تونغا','name_fr' => 'Tonga','name_en' => 'Tonga','code' => 'to','created_at' => NULL,'updated_at' => NULL),
            array('id' => '207','name' => 'ديك رومي','name_fr' => 'dinde','name_en' => 'Turkey','code' => 'tr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '208','name' => 'ترينداد وتوباغو','name_fr' => 'Trinité-et-Tobago','name_en' => 'Trinidad and Tobago','code' => 'tt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '209','name' => 'توفالو','name_fr' => 'Tuvalu','name_en' => 'Tuvalu','code' => 'tv','created_at' => NULL,'updated_at' => NULL),
            array('id' => '210','name' => 'تايوان','name_fr' => 'Taïwan','name_en' => 'Taiwan','code' => 'tw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '211','name' => 'تنزانيا','name_fr' => 'Tanzanie','name_en' => 'Tanzania','code' => 'tz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '212','name' => 'أوكرانيا','name_fr' => 'Ukraine','name_en' => 'Ukraine','code' => 'ua','created_at' => NULL,'updated_at' => NULL),
            array('id' => '213','name' => 'أوغندا','name_fr' => 'Ouganda','name_en' => 'Uganda','code' => 'ug','created_at' => NULL,'updated_at' => NULL),
            array('id' => '214','name' => 'أوروغواي','name_fr' => 'Uruguay','name_en' => 'Uruguay','code' => 'uy','created_at' => NULL,'updated_at' => NULL),
            array('id' => '215','name' => 'أوزبكستان','name_fr' => 'Ouzbékistan','name_en' => 'Uzbekistan','code' => 'uz','created_at' => NULL,'updated_at' => NULL),
            array('id' => '216','name' => 'سانت فنسنت وجزر غرينادين','name_fr' => 'Saint-Vincent-et-les-Grenadines','name_en' => 'Saint Vincent and the Grenadines','code' => 'vc','created_at' => NULL,'updated_at' => NULL),
            array('id' => '217','name' => 'فنزويلا','name_fr' => 'Venezuela','name_en' => 'Venezuela','code' => 've','created_at' => NULL,'updated_at' => NULL),
            array('id' => '218','name' => 'جزر العذراء البريطانية)','name_fr' => 'Îles vierges britanniques','name_en' => 'Virgin Islands (British)','code' => 'vg','created_at' => NULL,'updated_at' => NULL),
            array('id' => '219','name' => 'جزر فيرجن (الولايات المتحدة)','name_fr' => 'Îles Vierges (États-Unis)','name_en' => 'Virgin Islands (U.S.)','code' => 'vi','created_at' => NULL,'updated_at' => NULL),
            array('id' => '220','name' => 'فيتنام','name_fr' => 'Viet Nam','name_en' => 'Viet Nam','code' => 'vn','created_at' => NULL,'updated_at' => NULL),
            array('id' => '221','name' => 'فانواتو','name_fr' => 'Vanuatu','name_en' => 'Vanuatu','code' => 'vu','created_at' => NULL,'updated_at' => NULL),
            array('id' => '222','name' => 'واليس وفوتونا','name_fr' => 'Wallis et Futuna','name_en' => 'Wallis and Futuna','code' => 'wf','created_at' => NULL,'updated_at' => NULL),
            array('id' => '223','name' => 'ساموا','name_fr' => 'Samoa','name_en' => 'Samoa','code' => 'ws','created_at' => NULL,'updated_at' => NULL),
            array('id' => '224','name' => 'اليمن','name_fr' => 'Yémen','name_en' => 'Yemen','code' => 'ye','created_at' => NULL,'updated_at' => NULL),
            array('id' => '225','name' => 'مايوت','name_fr' => 'Mayotte','name_en' => 'Mayotte','code' => 'yt','created_at' => NULL,'updated_at' => NULL),
            array('id' => '226','name' => 'جنوب أفريقيا','name_fr' => 'Afrique du Sud','name_en' => 'South Africa','code' => 'za','created_at' => NULL,'updated_at' => NULL),
            array('id' => '227','name' => 'زامبيا','name_fr' => 'Zambie','name_en' => 'Zambia','code' => 'zm','created_at' => NULL,'updated_at' => NULL),
            array('id' => '228','name' => 'زائير (سابقة)','name_fr' => 'Zaïre (ancien)','name_en' => 'Zaire (former)','code' => 'zr','created_at' => NULL,'updated_at' => NULL),
            array('id' => '229','name' => 'زيمبابوي','name_fr' => 'Zimbabwe','name_en' => 'Zimbabwe','code' => 'zw','created_at' => NULL,'updated_at' => NULL),
            array('id' => '230','name' => 'الولايات المتحدة','name_fr' => 'les États-Unis d\'Amérique','name_en' => 'United States of America','code' => 'us','created_at' => NULL,'updated_at' => NULL)
        );

        $lastCountries = [];
        foreach ($countries as $country){
            $lastCountries [] = [
                'id' => $country['id'],
                'name_en' => $country['name_en'],
                'name_ar' => $country['name']
            ];
        }
        DB::table('countries')->insert($lastCountries);
    }
}
