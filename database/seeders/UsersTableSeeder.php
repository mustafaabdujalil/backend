<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create demo admin
        User::create([
           'name' => 'Admin',
           'user_name' => 'admin',
           'email' => 'admin@gift.com',
           'password' => Hash::make('password'),
           'phone' => '01234567899',
            'role' => User::ADMIN_ROLE
        ]);

        // create demo client
        User::create([
            'name' => 'client',
            'user_name' => 'client',
            'email' => 'client@gift.com',
            'password' => Hash::make('password'),
            'phone' => '01234567890',
            'role' => User::CLIENT_ROLE
        ]);
    }
}
