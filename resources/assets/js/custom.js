$("#country").on('change',function () {
    var countryId = $(this).val();
    $('#city option').remove();
    $.ajax({
        type: 'get',
        url: '/get-cities/' + countryId,
        success: function(response) {
            $.each(response, function(key, value) {
                $('#city')
                    .append($('<option>', { value : value.id })
                        .text(value.name));
            });
        }
    });
});

$(".remove_item_btn").on('click', function (){
    var url = $(this).attr('data-url');
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: url,
        data: {
            id: id,
            _method: 'DELETE',
            _token:  $("meta[name='csrf-token']").attr('content')
        },
        success: function(response) {
            location.reload();
        }
    });
});

$(".language_drop").on('change',function (){
    window.location.href = $('option:selected', this).attr('data-url');
});
