@extends('layouts.master')
@section('title')
    {{ __('Login') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'loginbackground', 'title' => __('Login'), 'bannerImage' =>  null ])

    <!--================login Area =================-->
    <section class="login_area p_100">
        <div class="container">
            <div class="login_inner registerpageout">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login_title">
                            <h2>{{ __('log in your account') }}</h2>
                            <p>{{ __('Follow the steps below to create your account.') }}</p>
                        </div>
                        <form class="login_form row" method="post" action="{{ route('login') }}" data-parsley-validate>
                            @csrf
                            <div class="col-lg-12 form-group">
                                <input class="form-control @error('email') is-invalid @enderror" required name="email" type="email" placeholder="{{ __('Email') }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 form-group">
                                <input class="form-control @error('password') is-invalid @enderror" required name="password" type="password" placeholder="{{ __('Password') }}">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <h5><a href="{{ route('register') }}"> {{ __('New account') }}</a></h5>
                            </div>
                            <div class="col-lg-6 form-group">
                                @if(Route::has('password.request'))
                                    <h4>
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </h4>
                                @endif
                            </div>


                            <div class="col-lg-6 form-group">
                                <button type="submit" value="submit" class="btn subs_btn form-control registerbtn">{{ __('Login') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End login Area =================-->

@endsection
@section('script')
@endsection
