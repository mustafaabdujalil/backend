@extends('layouts.master')
@section('title')
    {{ __('Reset Password') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'loginbackground', 'title' => __('Reset Password'), 'bannerImage' =>  null ])

    <!--================login Area =================-->
    <section class="login_area p_100">
        <div class="container">
            <div class="login_inner registerpageout">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login_title">
                            <h2>{{ __('Reset Password') }}</h2>
                        </div>
                        <form class="login_form row" method="post" action="{{ route('password.email') }}" data-parsley-validate>
                            @csrf
                            <div class="col-lg-12 form-group">
                                <input class="form-control @error('email') is-invalid @enderror" required name="email" type="email" placeholder="{{ __('Email') }}">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-lg-6 form-group">
                                <button type="submit" value="submit" class="btn subs_btn form-control registerbtn">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End login Area =================-->

@endsection
@section('script')
@endsection
