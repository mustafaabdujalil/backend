@extends('layouts.master')
@section('title')
    {{ __('Register') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'loginbackground', 'title' => __('Create Account'), 'bannerImage' =>  null ])

    <!--================register Area =================-->
    <section class="login_area p_100">
        <div class="container">
            <div class="login_inner registerpageout">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login_title">
                            <h2>{{ __('Create Account') }}</h2>
                            <p>{{ __('Log in to your account to discovery all great features.') }}</p>
                        </div>
                        <form class="login_form row" method="post" action="{{ route('register') }}" data-parsley-validate>
                            @csrf
                            <div class="col-lg-6 form-group">
                                <input class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" required name="name" type="text" placeholder="{{ __('Name') }}">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <input class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required name="email" type="email" placeholder="{{ __('Email') }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <input class="form-control @error('user_name') is-invalid @enderror" value="{{ old('user_name') }}" required name="user_name" type="text" placeholder="{{ __('User Name') }}">
                                @error('user_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <input class="form-control @error('phone') is-invalid @enderror" data-parsley-length="[11 , 11]" value="{{ old('phone') }}" required name="phone" type="text" placeholder="{{ __('Phone') }}">
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <input class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" required name="password" type="password" placeholder="{{ __('Password') }}">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <input class="form-control @error('password_confirmation') is-invalid @enderror" value="{{ old('password_confirmation') }}" required name="password_confirmation" type="password" placeholder="{{ __('Re-Password') }}">
                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-lg-6 form-group">
                                <select class="form-control @error('role') is-invalid @enderror select2" name="role" id="role" required>
                                    <option value="client">{{ __('Client') }}</option>
                                    <option value="customer" selected>{{ __('Customer') }}</option>
                                </select>
                                @error('role')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <h5><a href="{{ route('login') }}"> {{ __('Have an account?') }}</a></h5>

                            </div>
                            <div class="col-lg-6 form-group">
                                <button type="submit" value="submit" class="btn subs_btn form-control registerbtn">{{ __('Register now') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End register Area =================-->

@endsection
@section('script')
@endsection
