@extends('layouts.master')
@section('title')
    {{ __('Cart') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'solid_banner_area loginbackground', 'title' => __('Start a card gift') ])

    <!--================Shopping Cart Area =================-->
    <section class="shopping_cart_area p_100">
        <div class="container">
            <form action="{{ route('order.store') }}" method="post" data-parsley-validate>
                @csrf
                <div class="row">
                    <div class="col-lg-8">
                        <div class="cart_items">
                            <h3>{{ __('Available gifts') }}</h3>
                            <div class="table-responsive-md">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">{{ __('Gift') }}</th>
                                        <th scope="col">{{ __('Qty') }}</th>
                                        <th scope="col">{{ __('Paid') }}</th>
                                        <th scope="col" style="width:100px;">{{ __('Share a gift') }}</th>
                                        <th scope="col">{{ __('Remaining') }}</th>
                                        <th>{{ __('Total') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($cartItems as $index => $item)
                                        <tr>
                                            <td>
                                                <a class="remove_item_btn" data-url="{{ route('cart.destroy',['cart' => $index]) }}" data-id="{{ $index }}" style="cursor:pointer;">
                                                    <img src="{{ asset('img/icon/close-icon.png') }}" title="{{ __('Remove') }}" alt="Remove">
                                                </a>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img src="{{ $item['image'] }}" alt="Gift image" width="100">
                                                    </div>
                                                    <div class="media-body">
                                                        <h4>{{ $item['name'] }}</h4>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><p>{{ $item['quantity'] }}</p></td>
                                            <td><p>{{ $item['paid'] }} {{ __('L.E') }}</p></td>
                                            <td>
                                                <input type="text" name="price[]" value="{{ $item['remaining'] }}" data-parsley-trigger="keyup"
                                                       data-parsley-type="digits"  placeholder="{{ $item['remaining'] }}" required data-parsley-min="1" data-parsley-max="{{ $item['remaining'] }}" class="form-control myshare">
                                                <input type="hidden" name="cartItemIndex[]" value="{{ $index }}">
                                            </td>
                                            <td><p>{{ $item['remaining'] }} {{ __('L.E') }}</p></td>
                                            <td><p>{{ $item['price'] }} {{ __('L.E') }}</p></td>
                                        </tr>
                                    @empty
                                        <strong style="position: relative;top: 115px;left: 315px;">No item added to cart yet</strong>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="cart_totals_area">
                            <h4>{{ __('Cart Totals') }}</h4>
                            <div class="cart_t_list">
                                <div class="media">
                                    <div class="d-flex">
                                        <h5>{{ __('My shared') }}</h5>
                                    </div>
                                    <div class="media-body">
                                        <h6>{{ cart()->total() }} {{ __('L.E') }}</h6>
                                    </div>
                                </div>
    {{--                                <div class="media">--}}
    {{--                                    <div class="d-flex">--}}
    {{--                                        <h5>{{ __('Description') }}</h5>--}}
    {{--                                    </div>--}}
    {{--                                    <div class="media-body">--}}
    {{--                                        <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model tex</p>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
                                <div class="media">
                                    <div class="d-flex">

                                    </div>
                                </div>
                            </div>
                            <div class="total_amount row m0 row_disable">
                                <div class="float-left">
                                    {{ __('Total') }}
                                </div>
                                <div class="float-right">
                                    {{ cart()->total() }} {{ __('L.E') }}
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn subs_btn form-control" {{ count(cart()->items()) <= 0 ? 'disabled' : '' }}>{{ __('Proceed to checkout') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!--================End Shopping Cart Area =================-->

@endsection
@section('script')
@endsection
