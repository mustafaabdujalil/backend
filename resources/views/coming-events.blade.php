<div class="promotion_area">
    <div class="feature_inner row m0">
        <div class="left_promotion">
            <div class="f_add_item">
                <div class="f_add_img"><img class="img-fluid" src="img/feature-add/img-1.jpg" alt=""></div>
                <div class="f_add_hover">
                    <div class="sale">Comming</div>
                    <h4>Aya & Mohamed <br />Engagement</h4>
                    <a class="add_btn" href="#">View gifts<i class="arrow_right"></i></a>
                </div>
            </div>
        </div>
        <div class="right_promotion">
            <div class="f_add_item right_dir">
                <div class="f_add_img"><img class="img-fluid" src="img/feature-add/img-2.jpg" alt=""></div>
                <div class="f_add_hover">
                    <div class="off">Two days left</div>
                    <h4>Ramy<br />Birthday</h4>
                    <a class="add_btn" href="#">View gifts <i class="arrow_right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
