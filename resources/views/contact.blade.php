@extends('layouts.master')
@section('title')
    {{ __('contact us') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'contactusbg', 'title' => __('contact us'), 'bannerImage' =>  $contact->getFirstMedia()])

    <!--================Contact Area =================-->
    <section class="contact_area p_100">
        <div class="container">
            <div class="contact_title">
                {!! $contact->contact ?? '' !!}
            </div>
            <div class="row contact_details">
                <div class="col-lg-4 col-md-6">
                    <div class="media">
                        <div class="d-flex">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            {!! $contact->address !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media">
                        <div class="d-flex">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <a href="tel:{{ $contact->phone_1 ?? '' }}">{{ $contact->phone_1 ?? '' }}</a>
                            <a href="tel:{{ $contact->phone_2 ?? '' }}">{{ $contact->phone_2 ?? '' }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="media">
                        <div class="d-flex">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                            <a href="mailto:{{ $contact->email_1 ?? '' }}">{{ $contact->email_1 ?? '' }}</a>
                            <a href="mailto:{{ $contact->email_2 ?? '' }}">{{ $contact->email_2 ?? '' }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact_form_inner">
                <h3>{{ __('Drop a Message') }}</h3>
                <form class="contact_us_form row" action="{{ route('contact.store') }}" method="post" id="contactForm" data-parsley-validate>
                    @csrf
                    <div class="form-group col-lg-4">
                        <input type="text" class="form-control @error('full_name') is-invalid @enderror" id="name" name="full_name" placeholder="{{ __('Full Name') }} *" value="{{ old('full_name') }}" required>
                        @error('full_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-4">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="{{ __('Email Address') }} *" value="{{ old('email') }}" required>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-4">
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" data-parsley-type="digits" placeholder="{{ __('Phone') }}" value="{{ old('phone') }}" data-parsley-length="[11, 11]" required>
                        @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-12">
                        <textarea class="form-control @error('message') is-invalid @enderror" name="message" id="message" rows="1" placeholder="{{ __('Type Your Message') }}..." required>{{ old('message') }}</textarea>
                        @error('message')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group col-lg-12">
                        <button type="submit" value="{{ __('submit') }}" class="btn update_btn form-control">{{ __('Send Message') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--================End Contact Area =================-->
@endsection
@section('script')
@endsection
