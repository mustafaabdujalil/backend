@extends('layouts.master')
@section('title')
    {{ __('private events') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'eventsbackground', 'title' => __('All private events'), 'bannerImage' =>  asset('img/banner/f-add-6.jpg') ])

    <!--================events Product Area =================-->
    <section class="no_sidebar_2column_area">
        <div class="container">
            <div class="two_column_product">
                <div class="row">
                    @forelse($events as $event)
                        <div class="col-lg-3 col-sm-6">
                        <div class="l_product_item">
                            <div class="l_p_img">
                                <img class="img-fluid" src="{{ $event->getFirstMedia() ?? asset('img/product/birthday.jpg') }}" style="max-width: 268px;max-height: 318px;" alt="{{ $event->name }}">
                                <h5 class="sale"><i class="fa fa-lock"></i> {{ $event->category->name ?? "" }}</h5>
                            </div>
                            <div class="l_p_text">
                                <ul>
                                    <li>
                                        <form action="{{ route('events.show', ['slug' => $event->slug]) }}" method="post">
                                            @csrf
                                            <div class="input-group mb-3">
                                                <input class="form-control" type="password" name="password" required placeholder="{{ __('Enter event password') }}"/>
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-success" type="submit" style="cursor:pointer;">{{ __('Enter') }}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                                <h4>{{ $event->name ?? "" }}</h4>
                                <h5>{{ $event->date ?? "" }}</h5>
                            </div>
                        </div>
                    </div>
                    @empty
                        <h4 style="margin: auto;; font-weight: bolder">{{ __('No events yet') }}</h4>
                    @endforelse
                </div>

                @if(count($events) > 0)
                    <nav aria-label="Page navigation example" class="pagination_area">
                        <ul class="pagination">
                            @for($i = 1 ; $i <= $events->lastPage(); $i++)
                                <li class="page-item"><a class="page-link" href="{{ $events->url($i) }}">{{ $i }}</a></li>
                            @endfor
                            @if($events->hasMorePages())
                                <li class="page-item next"><a class="page-link" href="{{ $events->nextPageUrl() }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            @endif
                        </ul>
                    </nav>
                @endif
            </div>
        </div>
    </section>
    <!--================End events Product Area =================-->
@endsection
@section('script')
@endsection
