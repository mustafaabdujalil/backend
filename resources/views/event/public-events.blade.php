@extends('layouts.master')
@section('title')
    {{ __('public events') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'eventsbackground', 'title' => __('All public events'), 'bannerImage' =>  asset('img/banner/f-add-6.jpg') ])

    <!--================events Product Area =================-->
    <section class="categories_product_main p_80">
        <div class="container">
            <div class="categories_main_inner">
                <div class="row row_disable">
                    <div class="col-lg-9 float-md-right">
                        <div class="c_product_grid_details">

                            @forelse($events as $event)
                                <div class="c_product_item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">
                                            <div class="c_product_img">
                                                <img class="img-fluid" src="{{ $event->image }}" style="max-width: 268px;max-height: 318px;" alt="{{ $event->name }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-6">
                                            <div class="c_product_text">
                                                <h5>{{ $event->name ?? "" }}</h5>
                                                <h6>{{ __('Event date') }} <span>{{ $event->date ?? "" }}</span></h6>
                                                <p>
                                                    {!! $event->description ?? "" !!}
                                                </p>
                                                <ul class="c_product_btn">
                                                    <li><a class="add_card_btn" href="{{ route('events.show', ['slug' => $event->slug]) }}">{{ __('View') }}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <h4 style="margin: auto;; font-weight: bolder">{{ __('No events yet') }}</h4>
                            @endforelse

                            @if($events)
                                <nav aria-label="Page navigation example" class="pagination_area">
                                    <ul class="pagination">
                                        @for($i = 1 ; $i <= $events->lastPage(); $i++)
                                            <li class="page-item"><a class="page-link" href="{{ $events->url($i) }}">{{ $i }}</a></li>
                                        @endfor
                                        @if($events->hasMorePages())
                                            <li class="page-item next"><a class="page-link" href="{{ $events->nextPageUrl() }}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                        @endif
                                    </ul>
                                </nav>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-3 float-md-right">
                        <div class="categories_sidebar">
                            @if(count($latestEvents) > 0)
                                <aside class="l_widgest l_p_categories_widget">
                                <div class="l_w_title">
                                    <h3>{{ __('latest events') }}</h3>
                                </div>
                                <ul class="navbar-nav">
                                    @foreach($latestEvents as $event)
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">
                                                {{ $event->name ?? "" }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </aside>
                            @endif
                            @if(count($completedGifts) > 0)
                                <aside class="l_widgest l_feature_widget">
                                <div class="l_w_title">
                                    <h3>{{ __('Completed Gifts') }}</h3>
                                </div>

                                @foreach($completedGifts as $completeGift)
                                    <div class="media">
                                        <div class="d-flex">
                                                <img src="{{ $completeGift->image }}" style="max-width: 100px;max-height: 70px;" alt="{{ $completeGift->name }}">
                                        </div>
                                        <div class="media-body">
                                            <h4>{{ $completeGift->name ?? "" }}</h4>
                                            <h5>{{ $completeGift->price ?? "" }}</h5>
                                        </div>
                                    </div>
                                @endforeach

                            </aside>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End events Product Area =================-->
@endsection
@section('script')
@endsection
