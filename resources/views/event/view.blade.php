@extends('layouts.master')
@section('title')
    {{ $event->name ?? __('event') }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'eventbackground', 'title' => $event->name ?? "", 'bannerImage' =>  $event->getFirstMedia() ])

    <!--================Product Details Area =================-->
    <section class="product_details_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="product_details_slider">
                        <img src="{{ $event->getFirstMedia() ?? asset('img/product/birthday.jpg') }}" class="{{ $event->name ?? __('event') }}"/>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="product_details_text">
                                <h3>{{ __('Welcome to our event') }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="product_details_text">
                                <h6>{{ __('Event date') }}: <span>{{ $event->date ?? "" }}</span></h6>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="product_details_text">
                                <h6>{{ __('Host name') }}: <span>{{ $event->host_name ?? $event->host_code }}</span></h6>
                            </div>
                        </div>
                    </div>
                    <div class="product_details_text">
                        {!! $event->description ?? "" !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Product Details Area =================-->
    <hr/>

    <!--================End Related Product Area =================-->
    <section class="related_product_area">
        <div class="container">
            <div class="related_product_inner">
                <h2 class="single_c_title giftstitle"><i class="fa fa-gift"></i> {{ __('The gifts I want') }}</h2>
                <div class="row">

                    @foreach($event->gifts as $gift)
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <a href="#">
                                        <img class="img-fluid" src="{{ $gift->getFirstMedia() ?? asset('img/product/birthday.jpg') }}" alt="">
                                        <h5 class="done">{{ $gift->status == 0 ? 'Not available' : '' }}</h5>
                                    </a>
                                </div>
                                <div class="l_p_text">
                                    <ul>
                                        <li><a class="add_cart_btn" href="{{ route('cart.create',['gift_id' => $gift->id]) }}">{{ __('Buy Now') }}</a></li>
                                        <li><a class="add_card_btn" href="{{ route('cart.create',['gift_id' => $gift->id]) }}">{{ __('Make a card') }}</a></li>
                                    </ul>
                                    <h4>{{ $gift->name ?? "" }}</h4>
                                    <h5>{{ $gift->price }}</h5>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <!--================End Related Product Area =================-->

@endsection
@section('script')
@endsection
