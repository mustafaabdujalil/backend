<section class="aboutcardgift">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img src="{{ $about->image }}" class="img-responsive"/>
            </div>
            <div class="col-sm-8">
                <h2>{{ $about->title }}</h2>
                {!! $about->content !!}
            </div>
        </div>
    </div>
</section>
