@extends('layouts.master')
@section('title')
    {{ __('home') }}
@endsection
@section('style')
@endsection
@section('content')
    <!--================Main Content Area =================-->
    <section class="home_sidebar_area">
        <div class="container">
            <div class="row row_disable">
                <div class="col-lg-9 float-md-right">
                    <div class="sidebar_main_content_area">

                        @include('search')

                        @include('slider')

                        @include('coming-events')

                        @include('home-about')

                        @include('latest-events')

                        @include('private-events')

                    </div>
                </div>

                @include('side-menu')

            </div>
        </div>
    </section>
    <!--================End Main Content Area =================-->

    <!--================World Wide Service Area =================-->
    @include('service')
    <!--================End World Wide Service Area =================-->
@endsection
@section('script')
@endsection
