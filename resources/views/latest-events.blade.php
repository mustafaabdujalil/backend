<section class="fillter_latest_product">
    <div class="container">
        <div class="single_c_title">
            <h2>{{ __('Our Latest Events') }}</h2>
        </div>
        <div class="fillter_l_p_inner">
            <ul class="fillter_l_p">
                <li class="active" data-filter="*"><a href="#">{{ __('All') }}</a></li>
                @foreach($eventsCategories as $category)
                    <li data-filter=".{{ $category->id }}"><a href="#">{{ $category->name }}</a></li>
                @endforeach
            </ul>
            <div class="row isotope_l_p_inner">
                @foreach($events as $event)
                    <div class="col-lg-4 col-md-4 col-sm-6 {{ $event->category_id }}">
                        <div class="l_product_item">
                            <div class="l_p_img">
                                <a href="#">
                                    <img class="img-fluid" src="{{ $event->image }}" alt="Image">
                                    {{-- @if($gift->status == \App\Constants\Constants::DONE_STATUS)
                                        <h5 class="done">{{ __('Done') }}</h5>
                                    @endif --}}
                                </a>
                            </div>
                            <div class="l_p_text">
                                <ul>
                                    <!-- <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li> -->
                                    <li><a class="add_cart_btn btn-danger" href="{{ route('events.show',['slug' => $event->slug]) }}">{{ __('View') }}</a></li>

                                    {{-- @if($gift->is_gift_card)
                                        <li><a class="add_card_btn" href="{{ route('cart.create',['gift_id' => $gift->id]) }}">{{ __('Make a card') }}</a></li>
                                    @endif --}}

                                    <!-- <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li> -->
                                </ul>
                                <h4>{{ $event->name }}</h4>
                                {{-- <h5>{{ $gift->price }} {{ __('L.E') }}</h5> --}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
