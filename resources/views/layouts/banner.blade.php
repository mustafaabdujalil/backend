<!--================Banner Area =================-->
<section class="solid_banner_area {{$class}}" style="background-image: url({{ $banner ?? asset('/img/banner/f-add-6.jpg') }}) !important;">
    <div class="container">
        <div class="solid_banner_inner">
            <h3>{{ $title }}</h3>
        </div>
    </div>
</section>
<!--================End Banner Area =================-->
