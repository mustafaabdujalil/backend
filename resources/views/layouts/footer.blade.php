<footer class="footer_area">
    <div class="container">
        <div class="footer_widgets">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-6">
                    <aside class="f_widget f_about_widget">
                        <img src="{{ asset('img/logo-1.png') }}" alt="" class="footerimage">
                        <p>{{ __('Now you can add your own occasion and choose the gifts that you like your friends to bring to you.') }}</p>
                        <ul>
                            <li><a href="{{ $contact->facebook ?? "/" }}" target="_blank"><i class="social_facebook"></i></a></li>
                            <li><a href="{{ $contact->twitter ?? "/" }}" target="_blank"><i class="social_twitter"></i></a></li>
                            <li><a href="{{ $contact->instagram ?? "/" }}" target="_blank"><i class="social_instagram"></i></a></li>
                            <li><a href="{{ $contact->youtube ?? "/" }}" target="_blank"><i class="social_youtube"></i></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3 col-md-4 col-6">
                    <aside class="f_widget link_widget f_info_widget">
                        <div class="f_w_title">
                            <h3>{{ __('Information') }}</h3>
                        </div>
                        <ul>
                            <li><a href="{{ route('about-us') }}">{{ __('About') }}</a></li>
                            @if($pages)
                                @foreach($pages as $page)
                                    @if(in_array($page->position, [\App\Models\Page::FOOTER_POSITION, \App\Models\Page::BOTH_POSITION]))
                                        <li><a href="{{ $page->url }}">{{ ltrim($page->name) }}</a></li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3 col-md-4 col-6">
                    <aside class="f_widget link_widget f_service_widget">
                        <div class="f_w_title">
                            <h3>{{ __('Customer Service') }}</h3>
                        </div>
                        <ul>
                            <li><a href="#">{{ __('My account') }}</a></li>
                            <li><a href="#">{{ __('Newsletter') }}</a></li>
                            <li><a href="{{ route('contact-us') }}">{{ __('contact us') }}</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-2 col-md-4 col-6">
                    <aside class="f_widget link_widget f_extra_widget">
                        <div class="f_w_title">
                            <h3>{{ __('Extras') }}</h3>
                        </div>
                        <ul>
                            <li><a href="#">{{ __('Add your Events') }}</a></li>
                            <li><a href="#">{{ __('Start card gifts') }}</a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
        <div class="footer_copyright">
            <h5>©  {{ __('All rights reserved') }} <i class="fa fa-heart-o" style="color: red; font-weight: bold" aria-hidden="true"></i> {{ __('by') }} <a href="https://ideasqr.com" target="_blank">IdeaSQR</a>
            </h5>
        </div>
    </div>
</footer>
