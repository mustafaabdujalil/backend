<!--================Menu Area =================-->
<header class="shop_header_area carousel_menu_area">
    <div class="carousel_top_header black_top_header row m0">
        <div class="container">
            <div class="carousel_top_h_inner">
                <div class="float-md-left">
                    <div class="top_header_left">
                        <div class="selector">
                            <select class="language_drop" name="locale" id="locale" style="width:300px;">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <option value='{{ $localeCode }}' data-url="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" data-image="{{ asset('img/icon/'.$localeCode.'.png') }}" data-imagecss="flag yt" data-title="{{ $properties['native'] }}" {{ app()->getLocale() == $localeCode ? 'selected' : '' }}>{{ $properties['native'] }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>
                <div class="float-md-right">
                    <ul class="account_list">
                        <li><a href="@auth{{ route('order.index') }}@endauth @guest{{ route('login') }}@endguest">{{ __('My orders') }}</a></li>
                        <!-- <li><a href="#">Wish List (0)</a></li> -->
                        <li><a href="{{ route('cart.index') }}">{{ __('Shopping Cart') }}</a></li>
                        @auth
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endauth

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="carousel_menu_inner">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/"><img src="{{ asset('img/logo-1.png') }}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>

                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="/">{{ __('home') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('about-us') }}">{{ __('About') }}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('how-to-use') }}">{{ __('how to use') }} </a></li>
                        <li class="nav-item dropdown submenu">
                            <a class="nav-link dropdown-toggle" href="{{ route('events.list', ['type' => \App\Models\Event::PUBLIC_EVENT]) }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('events') }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="{{ route('events.list', ['type' => \App\Models\Event::PUBLIC_EVENT]) }}">{{ __('public events') }}</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('events.list', ['type' => \App\Models\Event::PRIVATE_EVENT]) }}">{{ __('private events') }}</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('contact-us') }}">{{ __('contact us') }}</a></li>
                        @if($pages)
                            @foreach($pages as $page)
                                @if(in_array($page->position, [\App\Models\Page::HEADER_POSITION, \App\Models\Page::BOTH_POSITION]))
                                <li class="nav-item"><a class="nav-link" href="{{ $page->url }}">{{ ltrim($page->name) }}</a></li>
                                @endif
                           @endforeach
                        @endif
                    </ul>
                    <ul class="navbar-nav justify-content-end">
                        <li class="user_icon"><a href="@auth{{ route('profile') }}@endauth @guest{{ route('login') }}@endguest"><i class="icon-user icons"></i></a></li>
                        <li class="cart_cart"><a href="{{ route('cart.index') }}"><i class="icon-handbag icons"></i></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
<!--================End Menu Area =================-->
