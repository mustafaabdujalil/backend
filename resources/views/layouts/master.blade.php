<!doctype html>
<html lang="{{ app()->getLocale() | 'en' }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon" />
        <title>@yield('title', __('gift'))</title>
        @include('layouts.style')
    </head>
    <body class="home_sidebar">
        <div class="home_box">
            @include('layouts.header')
            @yield('content')
            @include('layouts.footer')
        </div>
        @include('layouts.modals')
        @include('layouts.scripts')
        {!! Toastr::message() !!}
    </body>
</html>
