<link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css"/>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
<link rel="stylesheet" href="https:////cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<style>
    .cart_cart a:before {
        content: "{{ count(cart()->items()) }}";
    }
</style>
@yield('style')

@if(app()->getLocale() == 'ar')
    <link rel="stylesheet" href="{{ asset('assets/css/rtl.css') }}">
@endif
