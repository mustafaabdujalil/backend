@extends('layouts.master')
@section('title')
    {{ $page->name ?? "" }}
@endsection
@section('style')
@endsection
@section('content')
    @include('layouts.banner',['class' => 'contactusbg', 'title' => $page->name, 'bannerImage' =>  $page->getFirstMedia() ])

    <section class="page-previewcss">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>{{ $page->name ?? "" }} </h2>
                    <p>{!! $page->details ?? "" !!}</p>
                </div>
            </div>

        </div>
    </section>

@endsection
@section('script')
@endsection
