<div class="home_sidebar_blog">
    <h3 class="single_title privateevent">{{ __('private events') }}</h3>
    <p class="enterpassword">{{ __('you need to enter event password') }}</p>
    <div class="row">
        @forelse($privateEvents as $event)
            <div class="col-lg-3 col-sm-6">
                <div class="l_product_item">
                    <div class="l_p_img">
                        <img class="img-fluid" src="{{ $event->image }}" style="max-width: 100%;max-height: 318px;" alt="{{ $event->name }}">
                        <h5 class="sale"><i class="fa fa-lock"></i> {{ $event->category->name ?? "" }}</h5>
                    </div>
                    <div class="l_p_text">
                        <ul>
                            <li>
                                <form action="{{ route('events.show', ['slug' => $event->slug]) }}" method="post">
                                    @csrf
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="password" name="password" required placeholder="{{ __('Enter event password') }}"/>
                                        <div class="input-group-prepend">
                                            <button class="btn btn-success" type="submit" style="cursor:pointer;">{{ __('Enter') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                        <h4>{{ $event->name ?? "" }}</h4>
                        <h5>{{ $event->date ?? "" }}</h5>
                    </div>
                </div>
            </div>
        @empty
        @endforelse
    </div>
</div>
