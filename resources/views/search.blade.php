<form action="{{ route('events.list') }}" method="get">
    <div class="advanced_search_area">
        <select class="selectpicker" name="category_id">
            <option selected disabled>{{ __('Search in events') }}</option>
            @foreach($eventsCategories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
        <div class="input-group">
            <input type="text" name="key" class="form-control" placeholder="{{ __('Search') }}" >
            <span class="input-group-btn">
                <button class="btn btn-secondary" type="submit"><i class="icon-magnifier icons"></i></button>
            </span>
        </div>
    </div>
</form>
