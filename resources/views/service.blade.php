<section class="world_service">
    <div class="container">
        <div class="world_service_inner">
            <div class="row">
                <div class="col-lg-3">
                    <div class="world_service_item">
                        <h4><img src="{{ asset('img/icon/world-icon-1.png') }}" alt="Special Service">{{ __('Special Service') }}</h4>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="world_service_item">
                        <h4><img src="{{ asset('img/icon/world-icon-2.png') }}" alt="247 Help Center">{{ __('247 Help Center') }}</h4>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="world_service_item">
                        <h4><img src="{{ asset('img/icon/world-icon-3.png') }}" alt="Safe Payment">{{ __('Safe Payment') }}</h4>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="world_service_item">
                        <h4><img src="{{ asset('img/icon/world-icon-4.png') }}" alt="Home Shopping">{{ __('Home Shopping') }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
