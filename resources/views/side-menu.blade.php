<div class="col-lg-3 float-md-right">
    <div class="left_sidebar_area">
        <aside class="l_widget l_categories_widget">
            <div class="l_title">
                <h3>{{ __('All events') }}</h3>
            </div>
            <ul>
                @foreach($eventsCategories as $category)
                    <li><a href="{{  route('events.list', ['category_id' => $category->id]) }}">{{ $category->name }}</a></li>
                @endforeach
            </ul>
        </aside>
        <aside class="l_widget l_supper_widget">
            <img class="img-fluid" src="{{ asset('img/banner1.jpg') }}" alt="Image">
            <h4>{{ __('All the gifts you want') }}</h4>
        </aside>

        @if(count($completedGifts) > 0)
            <aside class="l_widget l_feature_widget">
                <div class="verticalCarousel">
                    <div class="verticalCarouselHeader">
                        <div class="float-md-left">
                            <h3>{{ __('Completed gifts') }}</h3>
                        </div>
                        <div class="float-md-right">
                            <a href="#" class="vc_goUp"><i class="arrow_carrot-left"></i></a>
                            <a href="#" class="vc_goDown"><i class="arrow_carrot-right"></i></a>
                        </div>
                    </div>
                    <ul class="verticalCarouselGroup vc_list">
                        @foreach($completedGifts as $gift)
                            <li>
                                <div class="media">
                                    <div class="d-flex">
                                        <a href="#">
                                            <img src="{{ $gift->image }}" style="max-width: 100px" alt="Image">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#">
                                            <h4>{{ $gift->name }}</h4>
                                            <h5>{{ $gift->price }} {{ __('L.E') }}</h5>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </aside>
        @endif

        <aside class="l_widget l_news_widget">
            <h3>{{ __('newsletter') }}</h3>
            <p>{{ __('Sign up for our Newsletter!') }}</p>
            <div class="input-group">
                <form action="{{ route('subscribe') }}" method="post">
                    @csrf
                    <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" required placeholder="yourmail@domain.com">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <span class="input-group-btn">
                        <button class="btn btn-secondary subs_btn" type="submit">{{ __('Subscribe') }}</button>
                    </span>
                </form>
            </div>
        </aside>
        @guest
            <aside class="l_widget l_hot_widget">
                <h3>{{ __('Add your events') }}</h3>
                <p></p>
                <a class="discover_btn" href="{{ route('register') }}">{{ __('Add event now') }}</a>
            </aside>
        @endguest
    </div>
</div>
