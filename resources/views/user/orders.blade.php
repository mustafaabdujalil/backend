@extends('layouts.master')
@section('title')
    {{ __('My orders') }}
@endsection
@section('style')
    <style>
        tr > th {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    @include('layouts.banner',['class' => 'solid_banner_area loginbackground', 'title' => __('Start a card gift') ])

    <!--================Orers Area =================-->
    <section class="shopping_cart_area p_100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="cart_items">
                        <h3>Gifts</h3>
                        <div class="table-responsive-md">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">{{ __('Gift') }}</th>
                                    <th scope="col">{{ __('Date') }}</th>
                                    <th scope="col" style="width:130px;">{{ __('Host name') }}</th>
                                    <th scope="col">{{ __('Event name') }}</th>
                                    <th>{{ __('Event details') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($orders as $order)
                                        @foreach($order->items as $item)
                                            <tr>

                                                <td>
                                                    <a href="#">
                                                        <div class="media">
                                                            <div class="d-flex">
                                                                <img src="{{ $item->item->image }}" alt="Gift image" width="100">
                                                            </div>
                                                            <div class="media-body">
                                                                <h4>{{ $item->item->name_en }}</h4>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td><p>{{ $item->event->date }}</p></td>
                                                <td>{{ $item->event->host_name }}</td>
                                                <td><p>{{ $item->event->name }}</p></td>
                                                <td><a href="{{ route('events.show', ['slug' => $item->event->slug]) }}" class="btn update_btn form-control">{{ __('View') }}</a></td>
                                            </tr>
                                        @endforeach
                                    @empty
                                        <strong style="position: relative;top: 115px;left: 460px;">No orders created yet</strong>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Orers Area =================-->

@endsection
@section('script')
@endsection
