@extends('layouts.master')
@section('title')
    {{ __('profile') }}
@endsection
@section('style')
@endsection
@section('content')

    @include('layouts.banner',['class' => 'aboutusbg', 'title' => __('Profile'), 'bannerImage' =>  $user->getFirstMedia() ])


    <!--================Contact Area =================-->
    <section class="contact_area p_100">
        <div class="container">
            <form action="{{ route('update-profile') }}" method="post" data-parsley-validate>
                @csrf
                <div class="row contact_details">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" name="first_name" value="{{ old('first_name', $user->first_name) }}" required placeholder="{{ __('First Name') }} *">
                            @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name" value="{{ old('last_name', $user->last_name) }}" required placeholder="{{ __('Last Name') }} *">
                            @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email', $user->email) }}" required placeholder="{{ __('Email') }} *">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{old('phone',  $user->phone )}}" required placeholder="{{ __('Phone') }} *">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <select id="country" name="country_id" required class="form-control @error('country_id') is-invalid @enderror select2">
                                <option> -{{ __('Country') }}- </option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ isset($user->city) && $user->city->country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <select id="city" name="city_id" required class="form-control @error('city_id') is-invalid @enderror select2">
                                <option> -{{ __('City') }}- </option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ $user->city_id == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                                @endforeach
                            </select>
                            @error('city_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <input class="form-control @error('address') is-invalid @enderror" placeholder="Address" name="address" required value="{{ old('address', $user->address) }}">
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group col-lg-12">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password"  placeholder="{{ __('Password') }} *">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                    <div class="col-md-6 form-group">
                        <button type="submit" value="submit" class="btn btn-md subs_btn form-control registerbtn">{{ __('Update') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!--================End Contact Area =================-->
@endsection
@section('script')
@endsection
