<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ListsController;
use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\Auth\LogoutController;
use App\Http\Controllers\API\Website\HomeController;
use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\Dashboard\GiftController;
use App\Http\Controllers\API\Dashboard\HostController;
use App\Http\Controllers\API\Dashboard\PageController;
use App\Http\Controllers\API\Dashboard\AboutController;
use App\Http\Controllers\API\Dashboard\EventController;
use App\Http\Controllers\API\Dashboard\SliderController;
use App\Http\Controllers\API\Dashboard\CategoryController;
use App\Http\Controllers\API\Dashboard\ContactUsController;
use App\Http\Controllers\API\Dashboard\ParticipantController;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function(){

    #### Authentication routes #####
    Route::prefix('auth')->group( function () {
        Route::post('register', RegisterController::class);
        Route::post('login', LoginController::class);
    });

    ######### Routes for logged user #########
    Route::middleware(['auth:sanctum', 'localization'])->group( function () {

        #### Authentication routes #####
        Route::prefix('auth')->group( function () {
            Route::post('logout', LogoutController::class);
        });

        ######### Dashboard Routes #########
        Route::group(['prefix' => 'dashboard'], function(){
            Route::apiResource('sliders', SliderController::class);
            Route::apiResource('about-us', AboutController::class);
            Route::apiResource('contact-us', ContactUsController::class);
            Route::apiResource('pages', PageController::class);
            Route::apiResource('categories', CategoryController::class);
            Route::apiResource('hosts', HostController::class);
            Route::apiResource('events', EventController::class);
            Route::apiResource('gifts', GiftController::class);
            Route::apiResource('participants', ParticipantController::class);
        });


        #### Lists routes #####
        Route::prefix('list')->group( function () {
            Route::get('hosts', [ListsController::class,'hosts']);
        });

        Route::apiResource('user', UserController::class);


    });

        ######### website routes #########
        Route::middleware(['localization'])->group( function () {
            Route::get('/about-us', [AboutController::class, 'index']);
            Route::get('/contact-us', [ContactUsController::class, 'index']);
            Route::post('/contact-us', [ContactUsController::class, 'storeContactUsMessage']);
            Route::get('/home', [HomeController::class, 'index']);

            #### Lists routes #####
            Route::prefix('list')->group( function () {
                Route::get('pages', [ListsController::class,'pages']);
                Route::get('countries', [ListsController::class,'countries']);
                Route::get('cities', [ListsController::class,'cities']);
            });

        });

});
