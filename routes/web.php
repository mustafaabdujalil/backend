<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Models\Page;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){
    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('/contact-us', [ContactController::class, 'index'])->name('contact-us');
    Route::post('/contact/store', [ContactController::class, 'storeContact'])->name('contact.store');
    Route::get('/about-us', [AboutController::class, 'index'])->name('about-us');
    //Route::get('/how-to-use', [HomeController::class, 'howToUse'])->name('how-to-use');
    Route::redirect('/how-to-use', '/')->name('how-to-use');

    Auth::routes();

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/profile', [UserController::class, 'index'])->name('profile');
        Route::get('/edit-profile', [UserController::class, 'edit'])->name('edit-profile');
        Route::post('/update-profile', [UserController::class, 'update'])->name('update-profile');
        Route::resource('order',OrderController::class);

    });

    Route::get('/events', [EventController::class, 'list'])->name('events.list');
    Route::any('/events/{slug}', [EventController::class, 'show'])->name('events.show');

    Route::get('/get-cities/{country_id}', [CommonController::class, 'getCities'])->name('getCities');
    Route::post('/subscribe', [SubscriptionController::class, 'subscribe'])->name('subscribe');

    Route::resource('cart',CartController::class);

    // Routes of dynamic pages
    if(\Illuminate\Support\Facades\Schema::hasTable('pages')) {
        foreach(Page::all() as $page){
            Route::view($page->getRawOriginal('url') , 'page' , compact('page'));
        }
    }
});
