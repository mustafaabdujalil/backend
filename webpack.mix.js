const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/assets/css/font-awesome.min.css',
    'resources/assets/vendors/line-icon/css/simple-line-icons.css',
    'resources/assets/vendors/elegant-icon/style.css',
    'resources/assets/css/bootstrap.min.css',
    'resources/assets/vendors/revolution/css/settings.css',
    'resources/assets/vendors/revolution/css/layers.css',
    'resources/assets/vendors/revolution/css/navigation.css',
    'resources/assets/vendors/owl-carousel/owl.carousel.min.css',
    'resources/assets/vendors/bootstrap-selector/css/bootstrap-select.min.css',
    'resources/assets/vendors/vertical-slider/css/jQuery.verticalCarousel.css',
    'resources/assets/css/style.css',
    'resources/assets/css/responsive.css',
    'resources/assets/css/ltr.css',
    'resources/assets/css/custom.css',
], 'public/assets/css/app.css');

mix.scripts([
    'resources/assets/js/jquery-3.2.1.min.js',
    'resources/assets/js/popper.min.js',
    'resources/assets/js/bootstrap.min.js',
    'resources/assets/vendors/revolution/js/jquery.themepunch.tools.min.js',
    'resources/assets/vendors/revolution/js/jquery.themepunch.revolution.min.js',
    'resources/assets/vendors/revolution/js/extensions/revolution.extension.actions.min.js',
    'resources/assets/vendors/revolution/js/extensions/revolution.extension.video.min.js',
    'resources/assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js',
    'resources/assets/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js',
    'resources/assets/vendors/revolution/js/extensions/revolution.extension.navigation.min.js',
    'resources/assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js',
    'resources/assets/vendors/counterup/jquery.waypoints.min.js',
    'resources/assets/vendors/counterup/jquery.counterup.min.js',
    'resources/assets/vendors/owl-carousel/owl.carousel.min.js',
    'resources/assets/vendors/bootstrap-selector/js/bootstrap-select.min.js',
    'resources/assets/vendors/image-dropdown/jquery.dd.min.js',
    'resources/assets/vendors/isotope/imagesloaded.pkgd.min.js',
    'resources/assets/vendors/isotope/isotope.pkgd.min.js',
    'resources/assets/vendors/magnify-popup/jquery.magnific-popup.min.js',
    'resources/assets/vendors/vertical-slider/js/jQuery.verticalCarousel.js',
    'resources/assets/vendors/jquery-ui/jquery-ui.js',
    'resources/assets/js/theme.js',
    'resources/assets/js/custom.js'
], 'public/assets/js/app.js');
